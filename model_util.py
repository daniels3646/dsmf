from metaflow import FlowSpec, step, IncludeFile, Flow, get_metadata, metadata, Run
import matplotlib.pyplot as plt
import csv
import pandas as pd
import numpy as np
import os
import time
import copy
import sys
from functools import partial
from sklearn.metrics import log_loss,auc,roc_curve
from sklearn.metrics import r2_score
from sklearn.base import BaseEstimator, TransformerMixin, ClassifierMixin, clone
from sklearn.utils import check_array
from datetime import date
from .viz_util import basic_info_df
import json
from pathlib import Path
from numba import jit
from sklearn.preprocessing import FunctionTransformer
from sklearn.compose import make_column_transformer
from sklearn.pipeline import make_pipeline
from sklearn.pipeline import make_union
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedKFold, KFold, RepeatedKFold, GroupKFold, GridSearchCV, train_test_split, TimeSeriesSplit
from sklearn import metrics,linear_model
import lightgbm as lgb
from catboost import CatBoostRegressor, CatBoostClassifier
from xgboost import XGBRegressor,plot_importance
from itertools import product
import seaborn as sns

def add_autoincrement(render_func):
  # Keep track of unique <div/> IDs
  cache = {}
  def wrapped(chart, id="vega-chart", autoincrement=True):
    if autoincrement:
      if id in cache:
        counter = 1 + cache[id]
        cache[id] = counter
      else:
        cache[id] = 0
      actual_id = id if cache[id] == 0 else id + '-' + str(cache[id])
    else:
      if id not in cache:
        cache[id] = 0
      actual_id = id
    return render_func(chart, id=actual_id)
  # Cache will stay outside and
  return wrapped


@jit
def fast_auc(y_true, y_prob):
  """
  fast roc_auc computation: https://www.kaggle.com/c/microsoft-malware-prediction/discussion/76013
  """
  y_true = np.asarray(y_true)
  y_true = y_true[np.argsort(y_prob)]
  nfalse = 0
  auc = 0
  n = len(y_true)
  for i in range(n):
    y_i = y_true[i]
    nfalse += (1 - y_i)
    auc += y_i * nfalse
  auc /= (nfalse * (n - nfalse))
  return auc


def eval_auc(y_true, y_pred):
  """
  Fast auc eval function for lgb.
  """
  return 'auc', fast_auc(y_true, y_pred), True


def group_mean_log_mae(y_true, y_pred, types, floor=1e-9):
  """
  Fast metric computation for this competition: https://www.kaggle.com/c/champs-scalar-coupling
  Code is from this kernel: https://www.kaggle.com/uberkinder/efficient-metric
  """
  maes = (y_true-y_pred).abs().groupby(types).mean()
  return np.log(maes.map(lambda x: max(x, floor))).mean()


def train_model_regression(X, X_test, y, params, folds=None, model_type='lgb', eval_metric='mae', columns=None, plot_feature_importance=False, model=None,
                verbose=10000, early_stopping_rounds=200, n_estimators=50000, splits=None, n_folds=3):
  """
  A function to train a variety of regression models.
  Returns dictionary with oof predictions, test predictions, scores and, if necessary, feature importances.

  :params: X - training data, can be pd.DataFrame or np.ndarray (after normalizing)
  :params: X_test - test data, can be pd.DataFrame or np.ndarray (after normalizing)
  :params: y - target
  :params: folds - folds to split data
  :params: model_type - type of model to use
  :params: eval_metric - metric to use
  :params: columns - columns to use. If None - use all columns
  :params: plot_feature_importance - whether to plot feature importance of LGB
  :params: model - sklearn model, works only for "sklearn" model type

  """
  columns = X.columns if columns is None else columns
  X_test = X_test[columns]
  splits = folds.split(X) if splits is None else splits
  n_splits = folds.n_splits if splits is None else n_folds

  # to set up scoring parameters
  metrics_dict = {
      'mae': {'lgb_metric_name': 'mae',
      'catboost_metric_name': 'MAE',
      'sklearn_scoring_function': metrics.mean_absolute_error},
      'group_mae': {'lgb_metric_name': 'mae',
      'catboost_metric_name': 'MAE',
      'scoring_function': group_mean_log_mae},
      'mse': {'lgb_metric_name': 'mse',
      'catboost_metric_name': 'MSE',
      'sklearn_scoring_function': metrics.mean_squared_error}
    }


  result_dict = {}

  # out-of-fold predictions on train data
  oof = np.zeros(len(X))

  # averaged predictions on train data
  prediction = np.zeros(len(X_test))

  # list of scores on folds
  scores = []
  feature_importance = pd.DataFrame()

  # split and train on folds
  for fold_n, (train_index, valid_index) in enumerate(splits):
    if verbose:
      print(f'Fold {fold_n + 1} started at {time.ctime()}')
    if type(X) == np.ndarray:
      X_train, X_valid = X[columns][train_index], X[columns][valid_index]
      y_train, y_valid = y[train_index], y[valid_index]
    else:
      X_train, X_valid = X[columns].iloc[train_index], X[columns].iloc[valid_index]
      y_train, y_valid = y.iloc[train_index], y.iloc[valid_index]

    if model_type == 'lgb':
      model = lgb.LGBMRegressor(**params, n_estimators = n_estimators, n_jobs = -1)
      model.fit(X_train, y_train,
          eval_set=[(X_train, y_train), (X_valid, y_valid)], eval_metric=metrics_dict[eval_metric]['lgb_metric_name'],
          verbose=verbose, early_stopping_rounds=early_stopping_rounds)

      y_pred_valid = model.predict(X_valid)
      y_pred = model.predict(X_test, num_iteration=model.best_iteration_)

    if model_type == 'xgb':
      train_data = xgb.DMatrix(data=X_train, label=y_train, feature_names=X.columns)
      valid_data = xgb.DMatrix(data=X_valid, label=y_valid, feature_names=X.columns)

      watchlist = [(train_data, 'train'), (valid_data, 'valid_data')]
      model = xgb.train(dtrain=train_data, num_boost_round=20000, evals=watchlist, early_stopping_rounds=200, verbose_eval=verbose, params=params)
      y_pred_valid = model.predict(xgb.DMatrix(X_valid, feature_names=X.columns), ntree_limit=model.best_ntree_limit)
      y_pred = model.predict(xgb.DMatrix(X_test, feature_names=X.columns), ntree_limit=model.best_ntree_limit)

    if model_type == 'sklearn':
      model = model
      model.fit(X_train, y_train)

      y_pred_valid = model.predict(X_valid).reshape(-1,)
      score = metrics_dict[eval_metric]['sklearn_scoring_function'](y_valid, y_pred_valid)
      print(f'Fold {fold_n}. {eval_metric}: {score:.4f}.')
      print('')

      y_pred = model.predict(X_test).reshape(-1,)

    if model_type == 'cat':
      model = CatBoostRegressor(iterations=20000, eval_metric=metrics_dict[eval_metric]['catboost_metric_name'], **params,
                   loss_function=metrics_dict[eval_metric]['catboost_metric_name'])
      model.fit(X_train, y_train, eval_set=(X_valid, y_valid), cat_features=[], use_best_model=True, verbose=False)

      y_pred_valid = model.predict(X_valid)
      y_pred = model.predict(X_test)

    oof[valid_index] = y_pred_valid.reshape(-1,)
    if eval_metric != 'group_mae':
      scores.append(metrics_dict[eval_metric]['sklearn_scoring_function'](y_valid, y_pred_valid))
    else:
      scores.append(metrics_dict[eval_metric]['scoring_function'](y_valid, y_pred_valid, X_valid['type']))

    prediction += y_pred

    if model_type == 'lgb' and plot_feature_importance:
      # feature importance
      fold_importance = pd.DataFrame()
      fold_importance["feature"] = columns
      fold_importance["importance"] = model.feature_importances_
      fold_importance["fold"] = fold_n + 1
      feature_importance = pd.concat([feature_importance, fold_importance], axis=0)

  prediction /= n_splits
  print('CV mean score: {0:.4f}, std: {1:.4f}.'.format(np.mean(scores), np.std(scores)))

  result_dict['oof'] = oof
  result_dict['prediction'] = prediction
  result_dict['scores'] = scores

  if model_type == 'lgb':
    if plot_feature_importance:
      feature_importance["importance"] /= n_splits
      cols = feature_importance[["feature", "importance"]].groupby("feature").mean().sort_values(
        by="importance", ascending=False)[:50].index

      best_features = feature_importance.loc[feature_importance.feature.isin(cols)]

      plt.figure(figsize=(16, 12));
      sns.barplot(x="importance", y="feature", data=best_features.sort_values(by="importance", ascending=False));
      plt.title('LGB Features (avg over folds)');

      result_dict['feature_importance'] = feature_importance

  return result_dict



def train_model_classification(X, X_test, y, params, folds, model_type='lgb', eval_metric='auc', columns=None, plot_feature_importance=False, model=None,verbose=10000, early_stopping_rounds=200, n_estimators=50000, splits=None, n_folds=3, averaging='usual', n_jobs=-1):
  """
  A function to train a variety of classification models.
  Returns dictionary with oof predictions, test predictions, scores and, if necessary, feature importances.

  :params: X - training data, can be pd.DataFrame or np.ndarray (after normalizing)
  :params: X_test - test data, can be pd.DataFrame or np.ndarray (after normalizing)
  :params: y - target
  :params: folds - folds to split data
  :params: model_type - type of model to use
  :params: eval_metric - metric to use
  :params: columns - columns to use. If None - use all columns
  :params: plot_feature_importance - whether to plot feature importance of LGB
  :params: model - sklearn model, works only for "sklearn" model type

  """
  columns = X.columns if columns is None else columns
  n_splits = folds.n_splits if splits is None else n_folds
  X_test = X_test[columns]

  # to set up scoring parameters
  metrics_dict = {
    'auc': {'lgb_metric_name': eval_auc,
      'catboost_metric_name': 'AUC',
      'sklearn_scoring_function': metrics.roc_auc_score},
  }

  result_dict = {}
  if averaging == 'usual':
    # out-of-fold predictions on train data
    oof = np.zeros((len(X), 1))

    # averaged predictions on train data
    prediction = np.zeros((len(X_test), 1))

  elif averaging == 'rank':
    # out-of-fold predictions on train data
    oof = np.zeros((len(X), 1))

    # averaged predictions on train data
    prediction = np.zeros((len(X_test), 1))


  # list of scores on folds
  scores = []
  feature_importance = pd.DataFrame()

  # split and train on folds
  for fold_n, (train_index, valid_index) in enumerate(folds.split(X)):
    print(f'Fold {fold_n + 1} started at {time.ctime()}')
    if type(X) == np.ndarray:
      X_train, X_valid = X[columns][train_index], X[columns][valid_index]
      y_train, y_valid = y[train_index], y[valid_index]
    else:
      X_train, X_valid = X[columns].iloc[train_index], X[columns].iloc[valid_index]
      y_train, y_valid = y.iloc[train_index], y.iloc[valid_index]

    if model_type == 'lgb':
      model = lgb.LGBMClassifier(**params, n_estimators=n_estimators, n_jobs = n_jobs)
      model.fit(X_train, y_train,
        eval_set=[(X_train, y_train), (X_valid, y_valid)], eval_metric=metrics_dict[eval_metric]['lgb_metric_name'],
        verbose=verbose, early_stopping_rounds=early_stopping_rounds)

      y_pred_valid = model.predict_proba(X_valid)[:, 1]
      y_pred = model.predict_proba(X_test, num_iteration=model.best_iteration_)[:, 1]

    if model_type == 'xgb':
      train_data = xgb.DMatrix(data=X_train, label=y_train, feature_names=X.columns)
      valid_data = xgb.DMatrix(data=X_valid, label=y_valid, feature_names=X.columns)

      watchlist = [(train_data, 'train'), (valid_data, 'valid_data')]
      model = xgb.train(dtrain=train_data, num_boost_round=n_estimators, evals=watchlist, early_stopping_rounds=early_stopping_rounds, verbose_eval=verbose, params=params)
      y_pred_valid = model.predict(xgb.DMatrix(X_valid, feature_names=X.columns), ntree_limit=model.best_ntree_limit)
      y_pred = model.predict(xgb.DMatrix(X_test, feature_names=X.columns), ntree_limit=model.best_ntree_limit)

    if model_type == 'sklearn':
      model = model
      model.fit(X_train, y_train)

      y_pred_valid = model.predict(X_valid).reshape(-1,)
      score = metrics_dict[eval_metric]['sklearn_scoring_function'](y_valid, y_pred_valid)
      print(f'Fold {fold_n}. {eval_metric}: {score:.4f}.')
      print('')

      y_pred = model.predict_proba(X_test)

    if model_type == 'cat':
      model = CatBoostClassifier(iterations=n_estimators, eval_metric=metrics_dict[eval_metric]['catboost_metric_name'], **params,
                   loss_function=Logloss)
      model.fit(X_train, y_train, eval_set=(X_valid, y_valid), cat_features=[], use_best_model=True, verbose=False)

      y_pred_valid = model.predict(X_valid)
      y_pred = model.predict(X_test)

    if averaging == 'usual':

      oof[valid_index] = y_pred_valid.reshape(-1, 1)
      scores.append(metrics_dict[eval_metric]['sklearn_scoring_function'](y_valid, y_pred_valid))

      prediction += y_pred.reshape(-1, 1)

    elif averaging == 'rank':

      oof[valid_index] = y_pred_valid.reshape(-1, 1)
      scores.append(metrics_dict[eval_metric]['sklearn_scoring_function'](y_valid, y_pred_valid))

      prediction += pd.Series(y_pred).rank().values.reshape(-1, 1)

    if model_type == 'lgb' and plot_feature_importance:
      # feature importance
      fold_importance = pd.DataFrame()
      fold_importance["feature"] = columns
      fold_importance["importance"] = model.feature_importances_
      fold_importance["fold"] = fold_n + 1
      feature_importance = pd.concat([feature_importance, fold_importance], axis=0)

  print('prediction type' , type(prediction))
  prediction /= n_splits

  print('CV mean score: {0:.4f}, std: {1:.4f}.'.format(np.mean(scores), np.std(scores)))

  result_dict['oof'] = oof
  result_dict['prediction'] = prediction
  result_dict['scores'] = scores

  if model_type == 'lgb':
    if plot_feature_importance:
      feature_importance["importance"] /= n_splits
      cols = feature_importance[["feature", "importance"]].groupby("feature").mean().sort_values(
        by="importance", ascending=False)[:50].index

      best_features = feature_importance.loc[feature_importance.feature.isin(cols)]

      plt.figure(figsize=(16, 12));
      sns.barplot(x="importance", y="feature", data=best_features.sort_values(by="importance", ascending=False));
      plt.title('LGB Features (avg over folds)');

      result_dict['feature_importance'] = feature_importance
      result_dict['top_columns'] = cols

  return result_dict

class DummyTransform(BaseEstimator, TransformerMixin):
  def __init__(self, name="", verbose = 0):
    """
    Dummy transformer to make sure column is retained in dataset
    even though it does not need to be transformerd.
    """
    self.verbose = verbose
    self.name = name

  def fit(self, X, y=None):
    if self.verbose:
      print(f'Fit: DummyTransform for: {self.name}...')
    return self

  def transform(self, X, y=None):
    if self.verbose:
      print(f'Transform: DummyTransform for: {self.name}...')
    return X


class ReluTransform(BaseEstimator, TransformerMixin):
  def __init__(self, name="", verbose = 0):
    """
    Sets negative values to 0 like a Rectified Linear Unit (ReLU)
    (for things like revenue which never should be zero anyway)
    """
    self.verbose = verbose
    self.name = name

  def fit(self, X, y=None):
    if self.verbose:
      print(f'Fit: ReluTransform for: {self.name}...')
    return self

  def transform(self, X, y=None):
    if self.verbose:
      print(f'Transform: ReluTransform for: {self.name}...')
    return np.maximum(X, 0)


class NumericFill(BaseEstimator, TransformerMixin):
  def __init__(self, fill='ExtremeValue', name="", verbose = 0):
    """
    Fill missing numerical values with either -999 or the mean
    of the column.
    """
    self.verbose = verbose
    self.name = name
    self.fill=fill
    self._mean = None

  def fit(self, X, y=None):
    # coerce column to either numeric, or to nan:
    self._mean = pd.to_numeric(X, errors='coerce').mean()
    if self.verbose and self.fill=='mean':
      print(f'Fit: Filling numerical NaN {self.name} with \
          {self.fill}: {self._mean}...')
    if self.verbose and self.fill=='ExtremeValue':
      print(f'Fit: Filling numerical NaN {self.name} with \
          {self.fill}: -999...')
    return self

  def transform(self, X, y=None):
    X = X.copy()
    if self.fill == 'mean':
      if self.verbose:
        print(f'Transform: Filling numerical NaN {self.name} \
            with {self.fill} : {self._mean} ...')
      X = pd.to_numeric(X, errors='coerce').fillna(self._mean)

    elif self.fill =='ExtremeValue':
      if self.verbose:
        print(f'Transform: Filling numerical NaN \
            {self.name} with {self.fill} : -999 ...')
      X = pd.to_numeric(X, errors='coerce').fillna(-999)
    return X


class StandardScale(BaseEstimator, TransformerMixin):
  def __init__(self, name="", verbose = 0):
    """
    Scale numerical features to mean=0, sd=1
    """
    self.verbose = verbose
    self.name = name
    self._mean = None
    self._sd = None

  def fit(self, X, y=None):
    if self.verbose:
      print(f'Fit: StandarScaling {self.name}: \
          ({self._mean}, {self._sd}...')

    self._mean = pd.to_numeric(X, errors='coerce').mean()
    self._sd = pd.to_numeric(X, errors='coerce').std()
    return self

  def transform(self, X, y=None):
    if self.verbose:
      print(f'Transform: StandarScaling {self.name}: \
          ({self._mean}, {self._sd}...')

    X = X.copy()
    X = pd.to_numeric(X, errors='coerce').fillna(self._mean)
    X -= self._mean
    if self._sd > 0:
      X /= self._sd
    return X.astype(np.float32)


class LabelEncode(BaseEstimator, TransformerMixin):
  def __init__(self, name="", verbose = 0):
    """
    Safe LabelEncoder. Deals with missing values and values
    not seen in original column (labels these 'Missing' before label
    encoding).
    (sklearn's LabelEncoder honestly kind of sucks for this)
    """
    self.verbose = verbose
    self.name = name
    self.le = None
    self.mapping = None

  def fit(self, X, y=None):
    if self.verbose:
      print(f'Fit: Label Encoding {self.name} ...')
    # get all the labels of the categorical variables and add a dummy
    # label 'Missing' as a category for both NaN's and new labels
    # not seen in the training set.
    labels = X.append(pd.Series(['Missing'])).value_counts().index.tolist()
    # create the mapping from the feature names to labels 0, 1, 2, etc
    self.mapping = dict(zip(labels, range(len(labels)) ))
    return self

  def transform(self, X, y=None):
    if self.verbose:
      print(f'Transform: Label Encoding {self.name} ...')
    X = X.copy()
    # missing_idx is the default value for the dictionary lookup:
    missing_idx = self.mapping["Missing"]
    return X.fillna("Missing").astype(str).apply(
          lambda x: self.mapping.get(x, missing_idx)).astype(int)

class MissingFill(BaseEstimator, TransformerMixin):
  def __init__(self, name = "", verbose = 0 ):
    """
    Fills missing categorical values with the label "Missing"
    """
    self.verbose = verbose
    self.name = name

  def fit(self, X, y=None):
    if self.verbose:
      print(f'Fit: Filling categorical NaN \
          {self.name} with \"Missing\" ...')
    return self

  def transform(self, X, y=None):
    if self.verbose:
      print(f'Transform: Filling categorical NaN \
          {self.name} with \"Missing\"...')
    return X.fillna("Missing")


class FrequencyFill(BaseEstimator, TransformerMixin):
  def __init__(self,fillna=-999):
    self.fillna = fillna
    pass

  def fit(self, X, y=None):
    # X should be a dataframe with one column
    return self

  def transform(self, X, y=None):
    frequency_dict = X.iloc[:,0].value_counts(normalize=True).to_dict()
    X_freq = X.replace(frequency_dict)
    X_freq = X_freq.replace([np.inf, -np.inf], np.nan)
    X_freq = X_freq.fillna(self.fillna)
    X_freq.columns = list(map(lambda x: x+'_freq',X_freq.columns))
    self.col = list(X_freq.columns)
    return X_freq

  def get_feature_names(self):
    return self.col

class OneHot(BaseEstimator, TransformerMixin):
  def __init__(self, topx = None, name = "", verbose = 0):
    """
    One hot encodes column. Adds a column _na, and codes any label not
    seen in the training data as _na. Also makes sure all columns in the
    training data will get created in the transformed dataframe.
    If topx is given only encodes the topx most frequent labels,
    and labels everything else _na.
    """
    self.verbose = verbose
    self.topx = topx
    self.name = name

  def fit(self, X, y=None):
    if self.verbose:
      print(f'Fit: One-hot coding categorical variable {self.name}...')
    X = X.copy()
    if self.topx is None:
      # store the particular categories to be encoded:
      self.categories = X.unique()
      # Then do a simple pd.get_dummies to get the columns
      self.columns = pd.get_dummies(pd.DataFrame(X),
                     prefix = "",
                     prefix_sep = "",
                     dummy_na=True).columns
    else:
      # only take the topx most frequent categories
      self.categories = [x for x in X.value_counts()\
                       .sort_values(ascending=False)\
                       .head(self.topx).index]
      # set all the other categories to np.nan
      X.loc[~X.isin(self.categories)] = np.nan
      self.columns = pd.get_dummies(pd.DataFrame(X),
                       prefix = "",
                       prefix_sep = "",
                       dummy_na=True).columns
    return self

  def transform(self, X, y=None):
    if self.verbose:
      print(f'Transform: One-hot coding categorical \
          variable {self.name}...')

    X = X.copy()
    # set all categories not present during fit() to np.nan:
    X.loc[~X.isin(self.categories)] = np.nan

    # onehot encode using pd.get_dummies
    X_onehot = pd.get_dummies(pd.DataFrame(X), prefix = "",
                 prefix_sep = "", dummy_na=True)

    # add in columns missing in transform() that were present during fit()
    missing_columns = set(self.columns) - set(X_onehot.columns)
    for col in missing_columns:
      X_onehot[col]=0
    # make sure columns are in the same order
    X_onehot = X_onehot[self.columns]
    assert set(X_onehot.columns) == set(self.columns)
    # save the column names so that they can be assigned by DataFrameMapper
    self._feature_names = X_onehot.columns
    return X_onehot

  def get_feature_names(self):
    # helper function for sklearn-pandas to assign right column names
    return self._feature_names


class LookupEncoder(BaseEstimator, TransformerMixin):
  def __init__(self, lookup_table, fill='mean', smoothing = 0,
         name = "", verbose=0):
    """
    Replaces every label in a categorical variable with the value given
    in the lookup_table.
    When this used to apply a mean encoding (a.k.a. target encoding),
    you can add smoothing to bias the labels with only few observations
    towards the mean.
    NaN can either be filled with 'mean' or 'ExtremeValue'
    """
    self.verbose = verbose
    self._dim = None
    self.lookup_table = lookup_table
    self.mapping = None
    self.fill = fill
    self._mean = None
    self.smoothing = smoothing
    self.name = name

  def fit(self, X, y=None, **kwargs):
    if self.verbose:
      print(f'Fit: Lookup table encoding {self.name}...')

    if len(self.lookup_table.columns)== 3:
      self.lookup_table.columns = ['label', 'encoding', 'count']
    elif len(self.lookup_table.columns)== 2:
      self.lookup_table.columns = ['label', 'encoding']

    if self.fill=='mean':
      assert len(self.lookup_table.columns)== 3
      self._mean = (
            np.sum(self.lookup_table['encoding'] *
                self.lookup_table['count']) /
                np.sum(self.lookup_table['count'])
      )

    if self.smoothing>0:
      assert len(self.lookup_table.columns)== 3
      # smoothing is used for mean encoding and biases the variables with
      # small counts towards the average
      # mean encoding. This is to prevent overfitting for categories with
      # small sample size.
      self.lookup_table['smooth_encoding'] = (
            (self.lookup_table['encoding'] * self.lookup_table['count']
            + self.mean*self.smoothing)
            / (self.lookup_table['count'] + self.smoothing)
      )

      self.mapping = pd.Series(
        self.lookup_table['smooth_encoding'].values.tolist(),
        index=self.lookup_table['label'].values.tolist()
      )
    else:
      self.mapping = pd.Series(
        self.lookup_table['encoding'].values.tolist(),
        index=self.lookup_table['label'].values.tolist()
      )

    self.fitted = True
    return self

  def transform(self, X, y=None):
    if self.verbose:
      print(f'Transform: Lookup table encoding {self.name}...')

    assert self.fitted == True

    mapped_column = X.map(self.mapping)
    if self.fill == 'mean':
      mapped_column.fillna(self._mean, inplace=True)
    elif self.fill =='ExtremeValue':
      mapped_column.fillna(-999, inplace=True)
    return mapped_column


class DoubleLookupEncoder(BaseEstimator, TransformerMixin):
  def __init__(self, lookup_table, fill='ExtremeValue', name="", verbose=0):
    """
    Replaces every label combination in two columns with the value given
    in the lookup_table.
    NaN can either be filled with 'mean' or 'ExtremeValue'.
    The second
    """
    self.lookup_table = lookup_tabl
    self.fill = fill
    self.name=name
    self.verbose = verbose
    self._mean = None
    self.mapping = None

  def fit(self, X, y, **kwargs):
    if self.verbose:
      print(f'Fit: Double Lookup table encoding {self.name}...')

    self.lookup_table.columns = ['label1', 'label2', 'encoding', 'count']
    if self.fill=='mean':
      self._mean = np.sum(
            (self.lookup_table['count'] * self.lookup_table['encoding'])
            / np.sum(self.lookup_table['count'])
      )
    self.fitted = True
    return self

  def transform(self, X, y = None):
    if self.verbose:
      print(f'Transform: Double Lookup table encoding {self.name}...')

    assert self.fitted == True
    assert isinstance(X, pd.DataFrame)

    X = X.copy()
    X.columns = ['label1', 'label2']
    # make sure version is always int:

    mapped_column = X.merge(self.lookup_table, how='left',
                   on=['label1', 'label2'])['encoding']

    if self.fill == 'mean':
      mapped_column.fillna(self._mean, inplace=True)
    elif self.fill =='ExtremeValue':
      mapped_column.fillna(-999, inplace=True)
    return mapped_column


class TargetEncoder(BaseEstimator, TransformerMixin):
  def __init__(self, smoothing=0, fill='ExtremeValue', name="", verbose=0,):
    """
    Replaces every label in a categorical column with the average value of
    the target (i.e. value that you are trying to predict).
    Add smoothing to bias values to the mean target value for labels
    with few observations.
    """
    self.smoothing = smoothing
    self.fill = fill
    self.name=name
    self.verbose = verbose
    self._mean = None
    self.mapping = None

  def fit(self, X, y, **kwargs):
    if self.verbose: print(f'Fit: Target Mean encoding {self.name}...')

    assert X.shape[0] == y.shape[0]
    assert isinstance(X, pd.Series)
    assert isinstance(y, pd.Series)

    combined = pd.concat([X, y], axis=1, ignore_index=True)
    combined.columns = ['label', 'target']

    self._mean = y.mean()

    self.mapping = combined.groupby('label').target.\
        apply(lambda x: ((x.mean() * x.count()) +
                 self._mean*self.smoothing) /
                  (x.count()+self.smoothing))
    self.fitted = True
    return self

  def transform(self, X, y=None):
    if self.verbose:
      print(f'Transform: Target Mean encoding {self.name}...')
    assert self.fitted == True
    assert (y is None or X.shape[0] == y.shape[0])
    assert isinstance(X, pd.Series)

    combined = pd.DataFrame(X)
    combined.columns = ['label']
    combined['label_target_enc'] = combined['feat'].map(self.mapping)

    if self.fill == 'mean':
      combined['feat_target_enc'].fillna(self._mean, inplace=True)
    elif self.fill =='ExtremeValue':
      combined['feat_target_enc'].fillna(-999, inplace=True)
    return combined['feat_target_enc']

class KFoldTargetEncoderTrain(BaseEstimator, TransformerMixin):
  def __init__(self,colnames,targetName, n_fold=5, verbosity=True, discardOriginal_col=False):
    self.colnames = colnames
    self.targetName = targetName
    self.n_fold = n_fold
    self.verbosity = verbosity
    self.discardOriginal_col = discardOriginal_col

  def fit(self, X, y=None):
    return self

  def transform(self,X):
    assert(type(self.targetName) == str)
    assert(type(self.colnames) == str)
    assert(self.colnames in X.columns)
    assert(self.targetName in X.columns)

    mean_of_target = X[self.targetName].mean()
    kf = KFold(n_splits = self.n_fold, shuffle = True, random_state=2019)

    col_mean_name = self.colnames + '_' + 'Kfold_Target_Enc'
    X[col_mean_name] = np.nan

    for tr_ind, val_ind in kf.split(X):
      X_tr, X_val = X.iloc[tr_ind], X.iloc[val_ind]
      X.loc[X.index[val_ind], col_mean_name] = X_val[self.colnames].map(X_tr.groupby(self.colnames)[self.targetName].mean())
      X[col_mean_name].fillna(mean_of_target, inplace = True)

    if self.verbosity:
      encoded_feature = X[col_mean_name].values
      print('Correlation between the new feature, {} and, {} is {}.'.format(col_mean_name,self.targetName,
      np.corrcoef(X[self.targetName].values, encoded_feature)[0][1]))

    if self.discardOriginal_col:
      X = X.drop(self.targetName, axis=1)

    return X

class Func(BaseEstimator, TransformerMixin):
  """Apply given transformation."""
  def __init__(self, transform_func):
    self.transform_func = transform_func

  def fit(self, X, y=None):
    return self

  def transform(self, X):
    assert callable(self.transform_func)
    transformed = self.transform_func(X) if callable(self.transform_func) else X
    self.cols=list(transformed.columns)
    return transformed

  def get_feature_names(self):
    return self.cols

class Func_1(BaseEstimator, TransformerMixin):
  """Apply given transformation."""
  def __init__(self, transform_func, param):
    self.transform_func = transform_func
    self.param = param

  def fit(self, X, y=None):
    return self

  def transform(self, X):
    assert callable(self.transform_func)
    transformed = self.transform_func(X,self.param) if callable(self.transform_func) else X
    self.cols=list(transformed.columns)
    return transformed

  def get_feature_names(self):
    return self.cols

# class Change_Col(BaseEstimator, TransformerMixin):
  # """Apply given transformation."""
  # def __init__(self, col, transform_func):
    # self.transform_func = transform_func
    # self.col = col

  # def fit(self, X, y=None):
    # return self

  # def transform(self, X):
    # assert callable(self.transform_func)
    # transformed = X
    # transformed[self.col] = self.transform_func(X) if callable(self.transform_func) else X
    # self.cols=list(transformed.columns)
    # return transformed

  # def get_feature_names(self):
    # return self.cols

class combine(Func):
  def __init__(self):
    super().__init__(lambda x: ((x.iloc[:,0].astype(str) + '_' + x.iloc[:,1].astype(str)).to_frame(x.columns[0]+'_'+x.columns[1])))

