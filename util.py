from metaflow import FlowSpec, step, IncludeFile, Flow, get_metadata, metadata, Run
from sklearn.pipeline import make_pipeline, make_union, Pipeline
from sklearn.compose import ColumnTransformer,make_column_transformer
import csv
import pandas as pd
import numpy as np
import sklearn.preprocessing as pre
import time
import copy
import sys
import os
import io
import zipfile
import re
import random
from functools import partial
from sklearn.base import BaseEstimator, TransformerMixin, ClassifierMixin, clone
from sklearn.utils import check_array
from category_encoders.ordinal import OrdinalEncoder
from datetime import date
from .viz_util import basic_info_df
import dsmf
import json
from pathlib import Path
from typing import Any
from numba import jit
from collections import defaultdict
import torch
import matplotlib.pyplot as plt
from functools import reduce
import operator
from collections.abc import Iterable
import numbers
import pickle
from tensorflow import is_tensor

def seed_everything(seed: int):
  random.seed(seed)
  os.environ["PYTHONHASHSEED"] = str(seed)
  np.random.seed(seed)
  torch.manual_seed(seed)
  torch.cuda.manual_seed(seed)
  torch.backends.cudnn.deterministic = True

def get_memory_usage():
  return np.round(psutil.Process(os.getpid()).memory_info()[0]/2.**30, 2)

def sizeof_fmt(num, suffix='B'):
  for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
    if abs(num) < 1024.0:
      return "%3.1f%s%s" % (num, unit, suffix)
    num /= 1024.0
  return "%.1f%s%s" % (num, 'Yi', suffix)

def df_mem(df):
  return "{:>20}: {:>8}".format('df size',sizeof_fmt(df.memory_usage(index=True).sum()))

# Nicely formatted time string
def hms_string(sec_elapsed):
  h = int(sec_elapsed / (60 * 60))
  m = int((sec_elapsed % (60 * 60)) / 60)
  s = sec_elapsed % 60
  return "{}:{:>02}:{:>05.2f}".format(h, m, s)

def get_cols(file_path,project_settings):
  folder_path = project_settings['PATH']
  full_path = os.path.join(folder_path , file_path )
  # print(full_path)
  with open(full_path, 'r') as f:
    d_reader = csv.DictReader(f)
    cols = d_reader.fieldnames
  return cols

def add_datepart(df: pd.DataFrame, field_name: str,
         prefix: str = None, drop: bool = True, time: bool = True, date: bool = True):
  """
  Helper function that adds columns relevant to a date in the column `field_name` of `df`.
  from fastai: https://github.com/fastai/fastai/blob/master/fastai/tabular/transform.py#L55
  """
  field = df[field_name]
  prefix = ifnone(prefix, re.sub('[Dd]ate$', '', field_name))
  attr = ['Year', 'Month', 'Week', 'Day', 'Dayofweek', 'Is_month_end', 'Is_month_start']
  if date:
    attr.append('Date')
  if time:
    attr = attr + ['Hour', 'Minute']
  for n in attr:
    df[prefix + n] = getattr(field.dt, n.lower())
  if drop:
    df.drop(field_name, axis=1, inplace=True)
  return df

def ifnone(a: Any, b: Any) -> Any:
  """`a` if `a` is not None, otherwise `b`.
  from fastai: https://github.com/fastai/fastai/blob/master/fastai/core.py#L92"""
  return b if a is None else a

@jit
def qwk(a1, a2):
  """
  Source: https://www.kaggle.com/c/data-science-bowl-2019/discussion/114133#latest-660168

  :param a1:
  :param a2:
  :param max_rat:
  :return:
  """
  max_rat = 3
  a1 = np.asarray(a1, dtype=int)
  a2 = np.asarray(a2, dtype=int)

  hist1 = np.zeros((max_rat + 1, ))
  hist2 = np.zeros((max_rat + 1, ))

  o = 0
  for k in range(a1.shape[0]):
    i, j = a1[k], a2[k]
    hist1[i] += 1
    hist2[j] += 1
    o += (i - j) * (i - j)

  e = 0
  for i in range(max_rat + 1):
    for j in range(max_rat + 1):
      e += hist1[i] * hist2[j] * (i - j) * (i - j)

  e = e / a1.shape[0]

  return 1 - o / e


def eval_qwk_lgb(y_true, y_pred):
  """
  Fast cappa eval function for lgb.
  """

  y_pred = y_pred.reshape(len(np.unique(y_true)), -1).argmax(axis=0)
  return 'cappa', qwk(y_true, y_pred), True


def eval_qwk_lgb_regr(y_true, y_pred):
  """
  Fast cappa eval function for lgb.
  """
  y_pred[y_pred <= 1.12232214] = 0
  y_pred[np.where(np.logical_and(y_pred > 1.12232214, y_pred <= 1.73925866))] = 1
  y_pred[np.where(np.logical_and(y_pred > 1.73925866, y_pred <= 2.22506454))] = 2
  y_pred[y_pred > 2.22506454] = 3

  # y_pred = y_pred.reshape(len(np.unique(y_true)), -1).argmax(axis=0)

  return 'cappa', qwk(y_true, y_pred), True


class LGBWrapper_regr(object):
  """
  A wrapper for lightgbm model so that we will have a single api for various models.
  """

  def __init__(self):
    self.model = lgb.LGBMRegressor()

  def fit(self, X_train, y_train, X_valid=None, y_valid=None, X_holdout=None, y_holdout=None, params=None):
    if params['objective'] == 'regression':
      eval_metric = eval_qwk_lgb_regr
    else:
      eval_metric = 'auc'

    eval_set = [(X_train, y_train)]
    eval_names = ['train']
    self.model = self.model.set_params(**params)

    if X_valid is not None:
      eval_set.append((X_valid, y_valid))
      eval_names.append('valid')

    if X_holdout is not None:
      eval_set.append((X_holdout, y_holdout))
      eval_names.append('holdout')

    if 'cat_cols' in params.keys():
      cat_cols = [col for col in params['cat_cols'] if col in X_train.columns]
      if len(cat_cols) > 0:
        categorical_columns = params['cat_cols']
      else:
        categorical_columns = 'auto'
    else:
      categorical_columns = 'auto'

    self.model.fit(X=X_train, y=y_train,
            eval_set=eval_set, eval_names=eval_names, eval_metric=eval_metric,
            verbose=params['verbose'], early_stopping_rounds=params['early_stopping_rounds'],
            categorical_feature=categorical_columns)

    self.best_score_ = self.model.best_score_
    self.feature_importances_ = self.model.feature_importances_

  def predict(self, X_test):
    return self.model.predict(X_test, num_iteration=self.model.best_iteration_)


def eval_qwk_xgb(y_pred, y_true):
  """
  Fast cappa eval function for xgb.
  """
  # print('y_true', y_true)
  # print('y_pred', y_pred)
  y_true = y_true.get_label()
  y_pred = y_pred.argmax(axis=1)
  return 'cappa', -qwk(y_true, y_pred)


class LGBWrapper(object):
  """
  A wrapper for lightgbm model so that we will have a single api for various models.
  """

  def __init__(self):
    self.model = lgb.LGBMClassifier()

  def fit(self, X_train, y_train, X_valid=None, y_valid=None, X_holdout=None, y_holdout=None, params=None):

    eval_set = [(X_train, y_train)]
    eval_names = ['train']
    self.model = self.model.set_params(**params)

    if X_valid is not None:
      eval_set.append((X_valid, y_valid))
      eval_names.append('valid')

    if X_holdout is not None:
      eval_set.append((X_holdout, y_holdout))
      eval_names.append('holdout')

    if 'cat_cols' in params.keys():
      cat_cols = [col for col in params['cat_cols'] if col in X_train.columns]
      if len(cat_cols) > 0:
        categorical_columns = params['cat_cols']
      else:
        categorical_columns = 'auto'
    else:
      categorical_columns = 'auto'

    self.model.fit(X=X_train, y=y_train,
            eval_set=eval_set, eval_names=eval_names, eval_metric=eval_qwk_lgb,
            verbose=params['verbose'], early_stopping_rounds=params['early_stopping_rounds'],
            categorical_feature=categorical_columns)

    self.best_score_ = self.model.best_score_
    self.feature_importances_ = self.model.feature_importances_

  def predict_proba(self, X_test):
    if self.model.objective == 'binary':
      return self.model.predict_proba(X_test, num_iteration=self.model.best_iteration_)[:, 1]
    else:
      return self.model.predict_proba(X_test, num_iteration=self.model.best_iteration_)


class CatWrapper(object):
  """
  A wrapper for catboost model so that we will have a single api for various models.
  """

  def __init__(self):
    self.model = cat.CatBoostClassifier()

  def fit(self, X_train, y_train, X_valid=None, y_valid=None, X_holdout=None, y_holdout=None, params=None):

    eval_set = [(X_train, y_train)]
    self.model = self.model.set_params(**{k: v for k, v in params.items() if k != 'cat_cols'})

    if X_valid is not None:
      eval_set.append((X_valid, y_valid))

    if X_holdout is not None:
      eval_set.append((X_holdout, y_holdout))

    if 'cat_cols' in params.keys():
      cat_cols = [col for col in params['cat_cols'] if col in X_train.columns]
      if len(cat_cols) > 0:
        categorical_columns = params['cat_cols']
      else:
        categorical_columns = None
    else:
      categorical_columns = None

    self.model.fit(X=X_train, y=y_train,
            eval_set=eval_set,
            verbose=params['verbose'], early_stopping_rounds=params['early_stopping_rounds'],
            cat_features=categorical_columns)

    self.best_score_ = self.model.best_score_
    self.feature_importances_ = self.model.feature_importances_

  def predict_proba(self, X_test):
    if 'MultiClass' not in self.model.get_param('loss_function'):
      return self.model.predict_proba(X_test, ntree_end=self.model.best_iteration_)[:, 1]
    else:
      return self.model.predict_proba(X_test, ntree_end=self.model.best_iteration_)


class XGBWrapper(object):
  """
  A wrapper for xgboost model so that we will have a single api for various models.
  """

  def __init__(self):
    self.model = xgb.XGBClassifier()

  def fit(self, X_train, y_train, X_valid=None, y_valid=None, X_holdout=None, y_holdout=None, params=None):

    eval_set = [(X_train, y_train)]
    self.model = self.model.set_params(**params)

    if X_valid is not None:
      eval_set.append((X_valid, y_valid))

    if X_holdout is not None:
      eval_set.append((X_holdout, y_holdout))

    self.model.fit(X=X_train, y=y_train,
            eval_set=eval_set, eval_metric=eval_qwk_xgb,
            verbose=params['verbose'], early_stopping_rounds=params['early_stopping_rounds'])

    scores = self.model.evals_result()
    self.best_score_ = {k: {m: m_v[-1] for m, m_v in v.items()} for k, v in scores.items()}
    self.best_score_ = {k: {m: n if m != 'cappa' else -n for m, n in v.items()} for k, v in self.best_score_.items()}

    self.feature_importances_ = self.model.feature_importances_

  def predict_proba(self, X_test):
    if self.model.objective == 'binary':
      return self.model.predict_proba(X_test, ntree_limit=self.model.best_iteration)[:, 1]
    else:
      return self.model.predict_proba(X_test, ntree_limit=self.model.best_iteration)

class RegressorModel(object):
  """
  A wrapper class for classification models.
  It can be used for training and prediction.
  Can plot feature importance and training progress (if relevant for model).

  """

  def __init__(self, columns: list = None, model_wrapper=None):
    """

    :param original_columns:
    :param model_wrapper:
    """
    self.columns = columns
    self.model_wrapper = model_wrapper
    self.result_dict = {}
    self.train_one_fold = False
    self.preprocesser = None

  def fit(self, X: pd.DataFrame, y,
      X_holdout: pd.DataFrame = None, y_holdout=None,
      folds=None,
      params: dict = None,
      eval_metric='rmse',
      cols_to_drop: list = None,
      preprocesser=None,
      transformers: dict = None,
      adversarial: bool = False,
      plot: bool = True):
    """
    Training the model.

    :param X: training data
    :param y: training target
    :param X_holdout: holdout data
    :param y_holdout: holdout target
    :param folds: folds to split the data. If not defined, then model will be trained on the whole X
    :param params: training parameters
    :param eval_metric: metric for validataion
    :param cols_to_drop: list of columns to drop (for example ID)
    :param preprocesser: preprocesser class
    :param transformers: transformer to use on folds
    :param adversarial
    :return:
    """

    if folds is None:
      folds = KFold(n_splits=3, random_state=42)
      self.train_one_fold = True

    self.columns = X.columns if self.columns is None else self.columns
    self.feature_importances = pd.DataFrame(columns=['feature', 'importance'])
    self.trained_transformers = {k: [] for k in transformers}
    self.transformers = transformers
    self.models = []
    self.folds_dict = {}
    self.eval_metric = eval_metric
    n_target = 1
    self.oof = np.zeros((len(X), n_target))
    self.n_target = n_target

    X = X[self.columns]
    if X_holdout is not None:
      X_holdout = X_holdout[self.columns]

    if preprocesser is not None:
      self.preprocesser = preprocesser
      self.preprocesser.fit(X, y)
      X = self.preprocesser.transform(X, y)
      self.columns = X.columns.tolist()
      if X_holdout is not None:
        X_holdout = self.preprocesser.transform(X_holdout)

    for fold_n, (train_index, valid_index) in enumerate(folds.split(X, y, X['installation_id'])):

      if X_holdout is not None:
        X_hold = X_holdout.copy()
      else:
        X_hold = None
      self.folds_dict[fold_n] = {}
      if params['verbose']:
        print(f'Fold {fold_n + 1} started at {time.ctime()}')
      self.folds_dict[fold_n] = {}

      X_train, X_valid = X.iloc[train_index], X.iloc[valid_index]
      y_train, y_valid = y.iloc[train_index], y.iloc[valid_index]
      if self.train_one_fold:
        X_train = X[self.original_columns]
        y_train = y
        X_valid = None
        y_valid = None

      datasets = {'X_train': X_train, 'X_valid': X_valid, 'X_holdout': X_hold, 'y_train': y_train}
      X_train, X_valid, X_hold = self.transform_(datasets, cols_to_drop)

      self.folds_dict[fold_n]['columns'] = X_train.columns.tolist()

      model = copy.deepcopy(self.model_wrapper)

      if adversarial:
        X_new1 = X_train.copy()
        if X_valid is not None:
          X_new2 = X_valid.copy()
        elif X_holdout is not None:
          X_new2 = X_holdout.copy()
        X_new = pd.concat([X_new1, X_new2], axis=0)
        y_new = np.hstack((np.zeros((X_new1.shape[0])), np.ones((X_new2.shape[0]))))
        X_train, X_valid, y_train, y_valid = train_test_split(X_new, y_new)

      model.fit(X_train, y_train, X_valid, y_valid, X_hold, y_holdout, params=params)

      self.folds_dict[fold_n]['scores'] = model.best_score_
      if self.oof.shape[0] != len(X):
        self.oof = np.zeros((X.shape[0], self.oof.shape[1]))
      if not adversarial:
        self.oof[valid_index] = model.predict(X_valid).reshape(-1, n_target)

      fold_importance = pd.DataFrame(list(zip(X_train.columns, model.feature_importances_)),
                      columns=['feature', 'importance'])
      self.feature_importances = self.feature_importances.append(fold_importance)
      self.models.append(model)

    self.feature_importances['importance'] = self.feature_importances['importance'].astype(int)

    # if params['verbose']:
    self.calc_scores_()

    if plot:
      # print(classification_report(y, self.oof.argmax(1)))
      fig, ax = plt.subplots(figsize=(16, 12))
      plt.subplot(2, 2, 1)
      self.plot_feature_importance(top_n=20)
      plt.subplot(2, 2, 2)
      self.plot_metric()
      plt.subplot(2, 2, 3)
      plt.hist(y.values.reshape(-1, 1) - self.oof)
      plt.title('Distribution of errors')
      plt.subplot(2, 2, 4)
      plt.hist(self.oof)
      plt.title('Distribution of oof predictions');

  def transform_(self, datasets, cols_to_drop):
    for name, transformer in self.transformers.items():
      transformer.fit(datasets['X_train'], datasets['y_train'])
      datasets['X_train'] = transformer.transform(datasets['X_train'])
      if datasets['X_valid'] is not None:
        datasets['X_valid'] = transformer.transform(datasets['X_valid'])
      if datasets['X_holdout'] is not None:
        datasets['X_holdout'] = transformer.transform(datasets['X_holdout'])
      self.trained_transformers[name].append(transformer)
    if cols_to_drop is not None:
      cols_to_drop = [col for col in cols_to_drop if col in datasets['X_train'].columns]

      datasets['X_train'] = datasets['X_train'].drop(cols_to_drop, axis=1)
      if datasets['X_valid'] is not None:
        datasets['X_valid'] = datasets['X_valid'].drop(cols_to_drop, axis=1)
      if datasets['X_holdout'] is not None:
        datasets['X_holdout'] = datasets['X_holdout'].drop(cols_to_drop, axis=1)
    self.cols_to_drop = cols_to_drop

    return datasets['X_train'], datasets['X_valid'], datasets['X_holdout']

  def calc_scores_(self):
    print()
    datasets = [k for k, v in [v['scores'] for k, v in self.folds_dict.items()][0].items() if len(v) > 0]
    self.scores = {}
    for d in datasets:
      scores = [v['scores'][d][self.eval_metric] for k, v in self.folds_dict.items()]
      print(f"CV mean score on {d}: {np.mean(scores):.4f} +/- {np.std(scores):.4f} std.")
      self.scores[d] = np.mean(scores)

  def predict(self, X_test, averaging: str = 'usual'):
    """
    Make prediction

    :param X_test:
    :param averaging: method of averaging
    :return:
    """
    full_prediction = np.zeros((X_test.shape[0], self.oof.shape[1]))
    if self.preprocesser is not None:
      X_test = self.preprocesser.transform(X_test)
    for i in range(len(self.models)):
      X_t = X_test.copy()
      for name, transformers in self.trained_transformers.items():
        X_t = transformers[i].transform(X_t)

      if self.cols_to_drop is not None:
        cols_to_drop = [col for col in self.cols_to_drop if col in X_t.columns]
        X_t = X_t.drop(cols_to_drop, axis=1)
      y_pred = self.models[i].predict(X_t[self.folds_dict[i]['columns']]).reshape(-1, full_prediction.shape[1])

      # if case transformation changes the number of the rows
      if full_prediction.shape[0] != len(y_pred):
        full_prediction = np.zeros((y_pred.shape[0], self.oof.shape[1]))

      if averaging == 'usual':
        full_prediction += y_pred
      elif averaging == 'rank':
        full_prediction += pd.Series(y_pred).rank().values

    return full_prediction / len(self.models)

  def plot_feature_importance(self, drop_null_importance: bool = True, top_n: int = 10):
    """
    Plot default feature importance.

    :param drop_null_importance: drop columns with null feature importance
    :param top_n: show top n columns
    :return:
    """

    top_feats = self.get_top_features(drop_null_importance, top_n)
    feature_importances = self.feature_importances.loc[self.feature_importances['feature'].isin(top_feats)]
    feature_importances['feature'] = feature_importances['feature'].astype(str)
    top_feats = [str(i) for i in top_feats]
    sns.barplot(data=feature_importances, x='importance', y='feature', orient='h', order=top_feats)
    plt.title('Feature importances')

  def get_top_features(self, drop_null_importance: bool = True, top_n: int = 10):
    """
    Get top features by importance.

    :param drop_null_importance:
    :param top_n:
    :return:
    """
    grouped_feats = self.feature_importances.groupby(['feature'])['importance'].mean()
    if drop_null_importance:
      grouped_feats = grouped_feats[grouped_feats != 0]
    return list(grouped_feats.sort_values(ascending=False).index)[:top_n]

  def plot_metric(self):
    """
    Plot training progress.
    Inspired by `plot_metric` from https://lightgbm.readthedocs.io/en/latest/_modules/lightgbm/plotting.html

    :return:
    """
    full_evals_results = pd.DataFrame()
    for model in self.models:
      evals_result = pd.DataFrame()
      for k in model.model.evals_result_.keys():
        evals_result[k] = model.model.evals_result_[k][self.eval_metric]
      evals_result = evals_result.reset_index().rename(columns={'index': 'iteration'})
      full_evals_results = full_evals_results.append(evals_result)

    full_evals_results = full_evals_results.melt(id_vars=['iteration']).rename(columns={'value': self.eval_metric,
                                              'variable': 'dataset'})
    sns.lineplot(data=full_evals_results, x='iteration', y=self.eval_metric, hue='dataset')
    plt.title('Training progress')

class CategoricalTransformer(BaseEstimator, TransformerMixin):

  def __init__(self, cat_cols=None, drop_original: bool = False, encoder=OrdinalEncoder()):
    """
    Categorical transformer. This is a wrapper for categorical encoders.

    :param cat_cols:
    :param drop_original:
    :param encoder:
    """
    self.cat_cols = cat_cols
    self.drop_original = drop_original
    self.encoder = encoder
    self.default_encoder = OrdinalEncoder()

  def fit(self, X, y=None):

    if self.cat_cols is None:
      kinds = np.array([dt.kind for dt in X.dtypes])
      is_cat = kinds == 'O'
      self.cat_cols = list(X.columns[is_cat])
    self.encoder.set_params(cols=self.cat_cols)
    self.default_encoder.set_params(cols=self.cat_cols)

    self.encoder.fit(X[self.cat_cols], y)
    self.default_encoder.fit(X[self.cat_cols], y)

    return self

  def transform(self, X, y=None):
    data = copy.deepcopy(X)
    new_cat_names = [f'{col}_encoded' for col in self.cat_cols]
    encoded_data = self.encoder.transform(data[self.cat_cols])
    if encoded_data.shape[1] == len(self.cat_cols):
      data[new_cat_names] = encoded_data
    else:
      pass

    if self.drop_original:
      data = data.drop(self.cat_cols, axis=1)
    else:
      data[self.cat_cols] = self.default_encoder.transform(data[self.cat_cols])

    return data

  def fit_transform(self, X, y=None, **fit_params):
    data = copy.deepcopy(X)
    self.fit(data)
    return self.transform(data)

@step
def split(self,next_steps):
  """
  split (because for some reason you cant join and split in the same split)

  """
  self.next(*next_steps)

def numOfNaNs(df):
  nans_df = df.isna()
  nans_groups={}
  i_cols = ['V'+str(i) for i in range(1,340)]
  for col in df.columns:
    cur_group = nans_df[col].sum()
    try:
      nans_groups[cur_group].append(col)
    except:
      nans_groups[cur_group]=[col]
  for k,v in nans_groups.items():
    print('####### NAN count =',k)
    print(v)
  return nans_groups

def oneHot(client,list_cols,new_name,func):
  df = dd.concat(list_cols,axis=1)
  print('df columns', df.columns)
  de = DummyEncoder()
  new_df = de.fit_transform(df.to_frame().categorize(columns=df.columns[0]))
  return new_df

def applymapOnColumns(df,new_names,func):
  new_df = df.applymap(func).rename(columns=dict(zip(df.columns,new_names)))
  return new_df

def applyOnColumns(df,new_name,func,meta):
  print('df columns', df.columns)
  new_df = df.apply(lambda x: func(*list(map(lambda col: x[col],df.columns))),axis=1,meta=meta).rename(new_name).to_frame()
  return new_df

def functionOnColumns(client,list_cols,new_names,func):
  df = dd.concat(list_cols,axis=1)
  print('df columns', df.columns)
  new_df = func(df).rename(columns=dict(zip(df.columns,new_names)))
  return new_df

def combineMultiIndex(X):
  X.columns = ['_'.join(col).strip() for col in X.columns.values]
  return X

class merge_left( BaseEstimator, TransformerMixin ):
  #Class Constructor
  def __init__( self, df, merge_col):
    self.df = df
    self.merge_col = merge_col

  #Return self nothing else to do here
  def fit( self, X, y = None ):
    return self

  #Method that describes what we need this transformer to do
  def transform( self, X, y = None ):
    X = X.merge(self.df, on=self.merge_col, how='left')
    return X

def group_idx_value(groupby,index,value): # groupby - cols , index - rows , value - values
  df_concat = dd.concat([groupby,index,value],axis=1)
  final = pd.DataFrame()
  for name , group in df_concat.groupby(groupby.columns[0]):
    if final.empty:
      final = group.set_index(index.columns[0])[[value.columns[0]]].rename(columns={value.columns[0]:name})
    else:
      final = final.join(group.set_index(index.columns[0])[[value.columns[0]]].rename(columns={value.columns[0]:name}))
  print(final.head())
  return final

class StackingEstimator(BaseEstimator, TransformerMixin):
  def __init__(self, estimator):
    self.estimator = estimator

  def fit(self, X, y=None, **fit_params):
    self.estimator.fit(X, y, **fit_params)
    return self

  def transform(self, X):
    X = check_array(X)
    X_transformed = np.copy(X)
    # add class probabilities as a synthetic feature
    if issubclass(self.estimator.__class__, ClassifierMixin) and hasattr(self.estimator, 'predict_proba'):
      X_transformed = np.hstack((self.estimator.predict_proba(X), X))

    # add class prodiction as a synthetic feature
    X_transformed = np.hstack((np.reshape(self.estimator.predict(X), (-1, 1)), X_transformed))

    return X_transformed

# safe downcast
def sd(col, max_loss_limit=0.001, avg_loss_limit=0.001, na_loss_limit=0, n_uniq_loss_limit=0, fillna=0):
    """
    max_loss_limit - don't allow any float to lose precision more than this value. Any values are ok for GBT algorithms as long as you don't unique values.
                     See https://en.wikipedia.org/wiki/Half-precision_floating-point_format#Precision_limitations_on_decimal_values_in_[0,_1]
    avg_loss_limit - same but calculates avg throughout the series.
    na_loss_limit - not really useful.
    n_uniq_loss_limit - very important parameter. If you have a float field with very high cardinality you can set this value to something like n_records * 0.01 in order to allow some field relaxing.
    """
    is_float = str(col.dtypes)[:5] == 'float'
    na_count = col.isna().sum()
    n_uniq = col.nunique(dropna=False)
    try_types = ['float16', 'float32']

    if na_count <= na_loss_limit:
        try_types = ['int8', 'int16', 'float16', 'int32', 'float32']

    for type in try_types:
        col_tmp = col

        # float to int conversion => try to round to minimize casting error
        if is_float and (str(type)[:3] == 'int'):
            col_tmp = col_tmp.copy().fillna(fillna).round()

        col_tmp = col_tmp.astype(type)
        max_loss = (col_tmp - col).abs().max()
        avg_loss = (col_tmp - col).abs().mean()
        na_loss = np.abs(na_count - col_tmp.isna().sum())
        n_uniq_loss = np.abs(n_uniq - col_tmp.nunique(dropna=False))

        if max_loss <= max_loss_limit and avg_loss <= avg_loss_limit and na_loss <= na_loss_limit and n_uniq_loss <= n_uniq_loss_limit:
            return col_tmp

    # field can't be converted
    return col

class renamer():
  def __init__(self):
    self.d = dict()

  def __call__(self, x):
    if x not in self.d:
      self.d[x] = 0
      return x
    else:
      self.d[x] += 1
      return "%s_%d" % (x, self.d[x])

#df.rename(columns=renamer())

def reduce_mem_usage_sd(df, deep=True, verbose=False, obj_to_cat=True):
  numerics = ['int16', 'uint16', 'int32', 'uint32', 'int64', 'uint64', 'float16', 'float32', 'float64']
  start_mem = df.memory_usage(deep=deep).sum() / 1024 ** 2
  for col in df.columns:
    col_type = df[col].dtypes

    # collect stats
    na_count = df[col].isna().sum()
    n_uniq = df[col].nunique(dropna=False)

    # numerics
    if col_type in numerics:
      df[col] = sd(df[col])

    # strings
    if (col_type == 'object') and obj_to_cat:
      df[col] = df[col].astype('category')

    if verbose:
      print(f'Column {col}: {col_type} -> {df[col].dtypes}, na_count={na_count}, n_uniq={n_uniq}')
    new_na_count = df[col].isna().sum()
    if (na_count != new_na_count):
      print(f'Warning: column {col}, {col_type} -> {df[col].dtypes} lost na values. Before: {na_count}, after: {new_na_count}')
    new_n_uniq = df[col].nunique(dropna=False)
    if (n_uniq != new_n_uniq):
      print(f'Warning: column {col}, {col_type} -> {df[col].dtypes} lost unique values. Before: {n_uniq}, after: {new_n_uniq}')

  end_mem = df.memory_usage(deep=deep).sum() / 1024 ** 2
  percent = 100 * (start_mem - end_mem) / start_mem
  if verbose:
    print('Mem. usage decreased from {:5.2f} Mb to {:5.2f} Mb ({:.1f}% reduction)'.format(start_mem, end_mem, percent))
  return df

def clean_inf_nan(df):
  return df.replace([np.inf, -np.inf], np.nan)

class ToDummiesTransformer(BaseEstimator, TransformerMixin):
  """ A Dataframe transformer that provide dummy variable encoding
  """

  def transform(self, X, **transformparams):
    trans = pd.get_dummies(X).copy()
    return trans

  def fit(self, X, y=None, **fitparams):
    return self


class DropAllZeroTrainColumnsTransformer(BaseEstimator, TransformerMixin):
  """ A DataFrame transformer that provides dropping all-zero columns
  """

  def transform(self, X, **transformparams):
    trans = X.drop(self.cols_, axis=1).copy()
    return trans

  def fit(self, X, y=None, **fitparams):
    self.cols_ = X.columns[(X==0).all()]
    return self

class columnTransformerReframer(BaseEstimator, TransformerMixin):
  """Transforms preceding columnTransformer's output back into Dataframe"""
  def __init__(self, ct, cutoff_transformer_name=True):
    self.ct = ct
    self.cutoff_transformer_name = cutoff_transformer_name

  def fit(self, X, y=None):
    return self

  def transform(self, X):
    assert isinstance(X, np.ndarray)
    if self.cutoff_transformer_name:
      self.cols = [c.split('__')[1] for c in self.ct.get_feature_names()]
    else:
      self.cols = self.ct.get_feature_names()
    df = reduce_mem_usage_sd(pd.DataFrame(data=X, columns=self.cols).infer_objects(),obj_to_cat=True)
    return df

  def get_feature_names(self):
    return self.cols

  @classmethod
  def retain(cls, *column_t,remainder='drop',n_jobs=-1):
    ct = make_column_transformer(*column_t,remainder=remainder)
    return pipelineWrapper(make_pipeline(ct,cls(ct)))

class FeatureUnionReframer(BaseEstimator, TransformerMixin):
  """Transforms preceding FeatureUnion's output back into Dataframe"""
  def __init__(self, union, cutoff_transformer_name=True):
    self.union = union
    self.cutoff_transformer_name = cutoff_transformer_name

  def fit(self, X, y=None):
    return self

  def transform(self, X):
    assert isinstance(X, np.ndarray)
    if self.cutoff_transformer_name:
      self.cols = [c.split('__')[1] for c in self.union.get_feature_names()]
    else:
      self.cols = self.union.get_feature_names()
    df = reduce_mem_usage_sd(pd.DataFrame(data=X, columns=self.cols).infer_objects(),obj_to_cat=True)
    return df

  def get_feature_names(self):
    return self.cols

  @classmethod
  def retain(cls, *feature_union):
    """With this method a feature union will be returned as a pipeline
    where the first step is the union and the second is a transformer that
    re-applies the columns to the union's output"""
    union = make_union(*feature_union,n_jobs=-1)
    return pipelineWrapper(make_pipeline(union,cls(union)))

class new_pipeline(BaseEstimator, TransformerMixin):
  def __init__(self, pipe):
    self.pipe = pipe

  def fit(self, X, y=None):
    self.pipe = make_pipeline(*self.pipe)
    self.pipe.fit(X,y)
    return self

  def transform(self, X, y=None):
    transformed = self.pipe.transform(X)
    self.cols = self.pipe[-1].get_feature_names()
    return transformed

  def get_feature_names(self):
    return self.cols

class pipelineWrapper(BaseEstimator, TransformerMixin):
  def __init__(self, pipe):
    self.pipe = pipe

  def fit(self, X, y=None):
    self.pipe.fit(X,y)
    return self

  def transform(self, X, y=None):
    transformed = self.pipe.transform(X)
    self.cols = self.pipe[-1].get_feature_names()
    return transformed

  def get_feature_names(self):
    return self.cols

class PassthroughTransformer(BaseEstimator, TransformerMixin):
  def fit(self, X, y=None):
    return self

  def transform(self, X):
    self.cols = list(X.columns)
    return X

  def get_feature_names(self):
    return self.cols

class change_type( BaseEstimator, TransformerMixin ):
  """Apply given transformation."""
  def __init__(self, types):
    self.types = types

  def fit(self, X, y=None):
    return self

  def transform(self, X):
    transformed = reduce_mem_usage_sd(X,obj_to_cat=True).astype(self.types)
    self.cols=list(transformed.columns)
    return transformed

  def get_feature_names(self):
    return self.cols

class addFeatureNames(BaseEstimator, TransformerMixin):
  def __init__(self, stockTransformer, suffix='', args={}):
    assert stockTransformer
    self.stockTransformer = stockTransformer
    self.suffix = suffix
    self.args = args

  def fit(self, X, y=None):
    self.stockTransformer = self.stockTransformer(**self.args)
    self.stockTransformer.fit(X,y)
    return self

  def transform(self, X):
    transformed = self.stockTransformer.transform(X)
    self.cols = list(map(lambda x: x+self.suffix,X.columns))
    transformed = pd.DataFrame(data=transformed, columns=self.cols).infer_objects()
    return transformed

  def get_feature_names(self):
    return self.cols

def ordinal_encode(df1,df2,cols):
  final=pd.concat([df1,df2])
  for col in cols:
    lb=pre.LabelEncoder()
    lb.fit(final[col])
    final[col+'_oes']=lb.transform(final[col])
  df1=final[:len(df1)]
  df2=final[len(df1):]
  # df1.columns = ['{}{}'.format(c, '_oes' if c in cols else '') for c in df1.columns]
  # df2.columns = ['{}{}'.format(c, '_oes' if c in cols else '') for c in df2.columns]
  return df1,df2

def count(tr,tt,cols,all_num_features):
  for feature in cols:
    if feature + '_count' in tr.columns:
      tr.drop(feature + '_count', axis=1, inplace=True)
    if feature + '_count' in tt.columns:
      tt.drop(feature + '_count', axis=1, inplace=True)
    valcounts = tr[feature].append(tt[feature]).value_counts()
    tr = tr.join(valcounts.rename(feature + '_count'), on=feature, how='left')
    tt = tt.join(valcounts.rename(feature + '_count'), on=feature, how='left')
    if feature + '_count' not in all_num_features:
      all_num_features.append(feature + '_count')
  return tr, tt, all_num_features

def binning(tr,tt,cols):
  bin_counts = defaultdict(lambda: 20)
  bin_counts['RescuerID_count'] = 10

  for feature in cols:
    if '_binned' in feature:
      continue
    n_bins = bin_counts[feature]
    if n_bins:
      bins = np.unique(tr[feature].quantile(np.linspace(0, 1, n_bins)).values)
      for df in [tr, tt]:
        n_bins = bin_counts[feature]
        df[feature + '_binned'] = pd.cut(
          df[feature], bins=bins, duplicates='drop'
        ).cat.codes
      if feature + '_binned' not in cols:
        cols.append(feature + '_binned')
  return tr, tt, cols

def ordinal_encode2(tr,tt,cols):
  for feature in cols:
    encoder = pre.LabelEncoder()
    encoder.fit(tr[feature].append(tt[feature]))
    tr[feature + '_label'] = encoder.transform(tr[feature])
    tt[feature + '_label'] = encoder.transform(tt[feature])
  return tr, tt

def standard_scaled(tr,tt,cols):
  for feature in cols:
    scaler = pre.StandardScaler()
    scaler.fit(tr[feature].append(tt[feature]).astype(np.float64).values[:, np.newaxis])
    tr[feature + '_scaled'] = scaler.transform(tr[feature].astype(np.float64).values[:, np.newaxis])
    tt[feature + '_scaled'] = scaler.transform(tt[feature].astype(np.float64).values[:, np.newaxis])
  return tr, tt

def read_reduce(file_):
  return reduce_mem_usage_sd(pd.read_csv(file_),verbose=True,obj_to_cat=True)

# The function "text_to_wordlist" is from
# https://www.kaggle.com/currie32/quora-question-pairs/the-importance-of-cleaning-text
def text_to_wordlist(text, remove_stopwords=False, stem_words=False):
  from nltk.corpus import stopwords
  from nltk.stem import SnowballStemmer

  # Clean the text, with the option to remove stopwords and to stem words.

  # Convert words to lower case and split them
  text = text.lower().split()

  # Optionally, remove stop words
  if remove_stopwords:
    stops = set(stopwords.words("english"))
    text = [w for w in text if not w in stops]

  text = " ".join(text)

  # Clean the text
  text = re.sub(r"[^A-Za-z0-9^,!.\/'+-=]", " ", text)
  text = re.sub(r"what's", "what is ", text)
  text = re.sub(r"\'s", " ", text)
  text = re.sub(r"\'ve", " have ", text)
  text = re.sub(r"can't", "cannot ", text)
  text = re.sub(r"n't", " not ", text)
  text = re.sub(r"i'm", "i am ", text)
  text = re.sub(r"\'re", " are ", text)
  text = re.sub(r"\'d", " would ", text)
  text = re.sub(r"\'ll", " will ", text)
  text = re.sub(r",", " ", text)
  text = re.sub(r"\.", " ", text)
  text = re.sub(r"!", " ! ", text)
  text = re.sub(r"\/", " ", text)
  text = re.sub(r"\^", " ^ ", text)
  text = re.sub(r"\+", " + ", text)
  text = re.sub(r"\-", " - ", text)
  text = re.sub(r"\=", " = ", text)
  text = re.sub(r"'", " ", text)
  text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
  text = re.sub(r":", " : ", text)
  text = re.sub(r" e g ", " eg ", text)
  text = re.sub(r" b g ", " bg ", text)
  text = re.sub(r" u s ", " american ", text)
  text = re.sub(r"\0s", "0", text)
  text = re.sub(r" 9 11 ", "911", text)
  text = re.sub(r"e - mail", "email", text)
  text = re.sub(r"j k", "jk", text)
  text = re.sub(r"\s{2,}", " ", text)

  # Optionally, shorten words to their stems
  if stem_words:
    text = text.split()
    stemmer = SnowballStemmer('english')
    stemmed_words = [stemmer.stem(word) for word in text]
    text = " ".join(stemmed_words)

  # Return a list of words
  return(text)

def printFiles():
  my_dir = '.'
  for f in os.listdir(my_dir):
    path = os.path.join(my_dir, f)
    if os.path.isfile(path):
      print(path,os.path.getsize(path))

def get_filename_from_res(res):
  """
  Get filename from content-disposition
  """
  cd = res.headers.get('content-disposition')
  if not cd:
    return None
  fname = re.findall('filename=(.+)', cd)
  if len(fname) == 0:
    return None
  return fname[0]

def download_extract_zip(response):
  """
  Download a ZIP file and extract its contents in memory
  yields (filename, file-like object) pairs
  """
  a = {}
  with zipfile.ZipFile(io.BytesIO(response.content)) as thezip:
    for zipinfo in thezip.infolist():
      with thezip.open(zipinfo) as thefile:
        a[zipinfo.filename] = thefile.read().decode('utf-8')
  return a

def unzip_on_disk(unzip_script):
  startup_file = 'download.sh'
  with open(startup_file, 'w') as f:
    f.write(unzip_script)
  os.system('bash download.sh')

normal_unzip_script = """#! /bin/bash
for f in *.zip; do unzip $f; done
rm *.zip"""

def pckl_write(obj,fn):
  pickle.dump(obj, open(fn, 'wb'))

def pckle_load(fn):
  return pickle.load(open(fn, 'rb'))

def save(gen):
  for (type_,obj,name) in gen():
    if type_ == 'plt':
      obj.savefig(name)
      obj.show()
    elif type_ == 'altair':
      obj.save(name + '.html',format='html',webdriver='firefox')
    elif type_ == 'html':
      text_file = open(name, "w")
      text_file.write(str(obj))
      text_file.close()
    elif type_ == 'csv':
      obj.to_csv(name+'.csv')
    elif type_ == 'io':
      text_file = open(name, "w")
      # a = obj.getvalue()
      # print('getvalue is: ',a)
      text_file.write(obj.getvalue())
      text_file.close()
    elif type_ == 'dict':
      json_ = json.dumps(obj)
      f = open(name,"w")
      f.write(json_)
      f.close()

def save_huge_df(experiment,df,name,uid,size=1000):
  uid,size=uid_size
  df['idx'] = df.groupby([uid]).ngroup()
  (df[df.idx < size]).to_csv(experiment.experiment_dir + '/' + name + '.csv')

def sample_frac(df,uid,frac):
  df['idx'] = df.groupby([uid]).ngroup()
  size = df.idx.max() * frac
  return df[df.idx < size].drop('idx',axis=1)

def save_normal(step,attrname,cols=None):
  obj = get_var(step,attrname,cols)
  folder = 'downloaded'
  if not os.path.exists(folder):
    os.makedirs(folder)
  save_logic(obj,attrname,folder,cols)

def save_experiments(experiment,dct):
  for attrname, obj in dct.items():
    if isinstance(obj,list) and len(obj) < 1000:
      for i, item in enumerate(obj):
        save_logic(item,attrname+'_'+str(i),experiment.experiment_dir)
    else:
      save_logic(obj,attrname,experiment.experiment_dir)

def save_logic(obj,attrname,folder,cols=None):
  if isinstance(obj,pd.Series) or isinstance(obj,pd.DataFrame):
    obj.to_parquet(folder + '/' + attrname + '.parquet',index=False,engine='fastparquet',compression='GZIP')
  elif is_tensor(obj) or 'Tensor' in obj.__class__.__name__:
    pickle.dump(obj, open(folder + '/' + attrname + '.pickle', 'wb'))
  elif isinstance(obj, torch.Tensor):
    obj = obj.cpu().numpy()
    pickle.dump(obj, open(folder + '/' + attrname + '.pickle', 'wb'))
  elif hasattr(obj, '__name__') and obj.__name__ == 'matplotlib.pyplot':
    obj.savefig(folder + '/' + attrname + '.png')
  elif isinstance(obj,torch.nn.Module) or "torch.optim" in str(obj.__class__):
    torch.save(obj.state_dict(), folder + '/' + attrname + '.pth')
  elif isinstance(obj,dict):
    json_ = json.dumps(obj)
    f = open(folder + '/' + attrname + '.json',"w")
    f.write(json_)
    f.close()
  elif (obj.__class__.__module__ == 'builtins' and not isinstance(obj, Iterable)) or isinstance(obj, numbers.Number) or isinstance(obj,str):
    text_file = open(folder + '/' + attrname + '.txt', "w")
    text_file.write(str(obj))
    text_file.close()
  else:
    pickle.dump(obj, open(folder + '/' + attrname + '.pickle', 'wb'))

def rankAveraging(label_s,uid='uid'):
  from scipy.stats import rankdata
  import glob

  predict_list = []
  LABELS = label_s
  files = glob.glob('experiments/*/sub_df.csv')

  for file_ in files:
    if isinstance(LABELS,list):
      predict_list.append(pd.read_csv(file_)[LABELS].values)
    else:
      predict_list.append(np.reshape(pd.read_csv(file_)[LABELS].values,(-1,1)))

  print("Rank averaging on ", len(predict_list), " files")
  predictions = np.zeros_like(predict_list[0])
  for predict in predict_list:
    for i in range(predictions.shape[1]):
      print(predictions[:, i], rankdata(predict[:, i])/predictions.shape[0])
      predictions[:, i] = np.add(predictions[:, i], rankdata(predict[:, i])/predictions.shape[0])
  predictions /= len(predict_list)

  submission = pd.read_csv(files[0])[[uid]]
  submission[LABELS] = predictions
  submission.to_csv('rank_averaged_submission.csv', index=False)

def models_corr(label,draw=True):
  import glob

  files = glob.glob('experiments/*/sub_df.csv')
  print('files: ',files)

  def load_and_name(fn):
    df = pd.read_csv(fn, index_col=0)
    df.rename(columns={label:fn.split('/')[-2]},inplace=True)
    return df

  outs = list(map(load_and_name,files))
  concat_sub = pd.concat(outs, axis=1)
  concat_sub.reset_index(inplace=True)

  corr = concat_sub.iloc[:,1:].corr()
  if draw:
    import matplotlib.pyplot as plt
    import seaborn as sns
    mask = np.zeros_like(corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True

    # Set up the matplotlib figure
    plt.subplots()

    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(corr, mask=mask, cmap='prism', vmin=0.96, center=0, square=True, linewidths=1, annot=True, fmt='.4f')
    plt.xticks(rotation=0)
    plt.show()

  submission = concat_sub.iloc[:,0].to_frame()
  # get the data fields ready for stacking
  submission['m_median'] = concat_sub.iloc[:, 1:].median(axis=1)
  submission['m_mean'] = concat_sub.iloc[:, 1:].mean(axis=1)
  return concat_sub, submission

  # conc , sub = util.models_corr('target',draw=False)

  # submission['target'] = concat_sub['m_median']
  # submission.to_csv('median_stack.csv', index=False)

class exper(object):
  def __init__(self,cls,local=False,stage=''):
    self.local = local
    self.stage = stage
    self.cfg = dict((key, value) for key, value in vars(cls).items() if not callable(value) and not key.startswith('__'))
    if not self.local:
      os.system('pip3 install --upgrade pip')
      os.system('pip3 install --user git+https://github.com/ex4sperans/mag.git')
      os.system('pip3 install --user pyarrow')
      os.system('pip3 install --user fastparquet')
    from mag.experiment import Experiment
    import mag
    mag.use_custom_separator("-")
    self.experiment = Experiment(self.cfg, implicit_resuming=True)
  def __call__(self,obj):
    if isinstance(obj,dict):
      save_experiments(self.experiment,obj)
    elif isinstance(obj,tuple) and len(obj) == 2:
      self.experiment.register_result(obj[0], obj[1])
    else:
      print('sry not sure what to do with this: ',type(obj))
  def log(self,*objects):
    with self.experiment: print(*objects)
  def ram(self):
    with self.experiment: os.system('free -m')
  def save(self,string):
    print('ending experiment. saving to obj')
    if not self.local:
      os.system('apt-get autoclean')
      os.system('apt install awscli -y')
    os.system('test -f ' + self.experiment.experiment_dir + '/log &&' + ' mv ' + self.experiment.experiment_dir + '/' + 'log' + ' ' + self.experiment.experiment_dir + '/' + self.stage + '_' + 'log')
    os.system('test -f ' + self.experiment.experiment_dir + '/results.json &&' + ' mv ' + self.experiment.experiment_dir + '/' + 'results.json' + ' ' + self.experiment.experiment_dir + '/' + self.stage + '_' + 'results.json')
    os.system('aws s3 cp ./ s3://4hsxt6jjg3tg44rc-public/' + string + ' --recursive --exclude "*" --include "experiments/*"')

