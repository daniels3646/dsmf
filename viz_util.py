from metaflow import FlowSpec, Step, IncludeFile, Flow, get_metadata, metadata, Run, namespace ,Metaflow
import matplotlib.pyplot as plt
from scipy.stats import skew
import pandas as pd
import numpy as np
from featexp import univariate_plotter
from featexp import get_trend_stats
from featexp import get_univariate_plots
import os
import pickle
import json
import datetime as dt
import seaborn as sns
from tabulate import tabulate
from functools import reduce
import operator
from collections.abc import Iterable
import numbers
# import altair as alt
# alt.renderers.set_embed_options(theme='dark')
# from selenium import webdriver

def get_var(step,attrname,cols=None):
  flow = list(Metaflow())[0]
  run = flow.latest_successful_run
  print('taken from: ',run.id)
  if cols:
    df = getattr(run[step].task.data, attrname)[cols]
  else:
    df = getattr(run[step].task.data, attrname)
  if isinstance(df, pd.Series):
    df = df.to_frame()
  return df

def get_var_run(step,attrname,run_num):
  flow_name = list(Metaflow())[0].id
  run = Run(flow_name+'/'+run_num)
  # print('taken from: ',run.id)
  data = getattr(run[step].task.data, attrname)
  return data

# Regression chart, we will see more of this chart in the next class.
def chart_regression(pred, y):
  t = pd.DataFrame({'pred': pred, 'y': y.flatten()})
  t.sort_values(by=['y'], inplace=True)
  a = plt.plot(t['y'].tolist(), label='expected')
  b = plt.plot(t['pred'].tolist(), label='prediction')
  plt.ylabel('output')
  plt.legend()
  plt.show()

def featexp_univariate(train_col_list,target):
  (new_train , new_val) = get_val(train_col_list,target)
  get_univariate_plots(data=new_train, target_col=target.columns[0], features_list=[series.columns[0] for series in train_col_list], data_test=new_val)

def featexp_trend_stats(train_col_list,target):
  (new_train , new_val) = get_val(train_col_list,target)
  stats = get_trend_stats(data=new_train, target_col=target.columns[0], data_test=new_val)
  print(stats)

def concat_outs(inpt,dask=False):
  if dask:
    return dd.concat([x.load(dask=True) for x in list(inpt.values())],axis=1)
  else:
    return pd.concat([x.load() for x in list(inpt.values())],axis=1)

def basic_info(step,attrname,cols=None,html=False):
  df = get_var(step,attrname,cols)
  if html:
    html = df.to_html(max_rows=10000)
    #write html to file
    text_file = open("output.html", "w")
    text_file.write(html)
    text_file.close()
  else:
    print('columns: ' ,df.columns , 'num of columns: ', len(df.columns))
    print('index names: ', df.index.names)
    print('num of rows: ', len(df))
    print('dtypes: ', df.dtypes)
    print('head: ', prttPrnt(df.head(30)))
    print('tail: ', prttPrnt(df.tail(10)))

def see_outputs(task):
  class see_outputs(d6tflow.tasks.TaskData):
    def requires(self):
      return task()
    def run(self):
      print('outputs: ')
      list(map(lambda x: print('  ',x),self.input().keys()))
  return see_outputs()

def basic_info_df(df,cols=None,ispd=True):
  print(type(df))
  if cols:
    df = df[cols]
  else:
    df = df
  if not ispd:
    df = df.compute()
  print('columns: ' ,df.columns , 'num of columns: ', len(df.columns))
  print('index names: ', df.index.names)
  print('num of rows: ', len(df))
  print('head: ', prttPrnt(df.head(10)))
  print('tail: ', prttPrnt(df.tail(10)))
  prttPrnt(df.dtypes.to_frame('dtypes'))
  print('describe: ', df.describe())
  total_null = df.isna().sum().sort_values(ascending=False)
  percent = 100*(df.isna().sum()/df.isna().count()).sort_values(ascending=False)
  missing_data = pd.concat([total_null.to_frame('Total'), percent.to_frame('Percent')], axis=1)
  df.info()
  print('nunique: \n',df.nunique())
  prttPrnt(missing_data)

def extensive_info(step,attrname,cols=None):
  df = get_var(step,attrname,cols)
  profile = df.profile_report(title='Pandas Profiling Report')
  profile.to_file(output_file="output.html")

def pivot(task,cols=None):
  df = get_var(step,attrname,cols)
  profile = df.profile_report(title='Pandas Profiling Report')
  print('pivoting')
  pivot_ui(df)

def minimal(step,attrname,cols=None):
  df = get_var(step,attrname,cols)
  print('columns: ' ,df.columns , 'num of columns: ', len(df.columns))
  print('index names: ', df.index.names)
  print('num of rows: ', len(df))
  prttPrnt(df.dtypes.to_frame('dtypes'))
  print('describe: ', df.describe())
  total_null = df.isna().sum().sort_values(ascending=False)
  percent = 100*(df.isna().sum()/df.isna().count()).sort_values(ascending=False)
  missing_data = pd.concat([total_null, percent], axis=1, keys=['Total', 'Percent'])
  df.info()
  print('nunique: \n',df.nunique())
  prttPrnt(missing_data)


def basic_info_ser(ser,cols=None,pd=True):
  print(type(ser))
  if cols:
    ser = ser[cols]
  else:
    ser = ser
  if not pd:
    ser = ser.compute()
  print('name: ' ,ser.name)
  print('index names: ', ser.index.names)
  print('num of rows: ', len(ser))
  print('dtypes: ', ser.dtypes)
  # print('head: ', prttPrnt(ser.head(30)))
  # print('tail: ', prttPrnt(ser.tail(10)))
  print('head: ' , ser.head(30))
  print('tail: ' , ser.tail(10))

def prttPrnt(df):
  print(tabulate(df, headers='keys', tablefmt='psql'))

def missing_values(df):
  total = df.isnull().sum().sort_values(ascending = False)
  percent = (df.isnull().sum()/df.isnull().count()*100).sort_values(ascending = False)
  missing__train_data  = pd.concat([total, percent], axis=1, keys=['Total', 'Percent'])
  print(missing__train_data)

def plotsNaNunique(df):
  cols = df.columns
  col = 4
  row = len(cols)//4+1
  plt.figure(figsize=(20,row*5))
  idx = df[~df[cols[0]].isna()].index
  for i,v in enumerate(cols):
    plt.subplot(row,col,i+1)
    n = df[v].nunique()
    x = np.sum(df.loc[idx,v]!=df.loc[idx,v].astype(int))
    y = np.round(100*np.sum(df[v].isna())/len(df),2)
    t = 'int'
    if x!=0: t = 'float'
    plt.title(v+' has '+str(n)+' '+t+' and '+str(y)+'% nan')
    plt.yticks([])
    h = plt.hist(df.loc[idx,v],bins=100)
    if len(h[0])>1: plt.ylim((0,np.sort(h[0])[-2]))
  return plt

def skewed_feats(train,test):

  numeric_feats_train = train.select_dtypes(exclude=['object'])
  print('train')
  for col in numeric_feats_train.columns:
    print('col: {}, skew: {}'.format(col,skew(numeric_feats_train[col]).compute()))

  numeric_feats_test = test.select_dtypes(exclude=['object'])
  print('test')
  for col in numeric_feats_test.columns:
    print('col: {}, skew: {}'.format(col,skew(numeric_feats_test[col]).compute()))

def freq(inpt):
  reorder_dow_freq = inpt.value_counts()
  fig = plt.figure()
  reorder_dow_freq.plot(kind='bar', color='skyblue')
  plt.title('Frequency')
  plt.show()

def bar(inpt):
  # fig = plt.Figure()
  plt.bar(inpt.index, inpt ,color='lightpink')
  plt.xticks(rotation=90)
  plt.xlabel(inpt.name)
  plt.title('bar plot')
  return plt

def stem(inpt):
  inpt.iloc[:,0].compute().plot(title='stem of: {}'.format(col))
  plt.show()

def wrt(inpt,y,wrt,merged=False): # y and wrt should be strings
  client = Client()
  concat = df_concat(inpt,[y,wrt],merged)
  df_grouped = client.compute(concat.groupby(wrt)[y].sum()).result()
  client.close()
  plt.plot(df_grouped)
  plt.title('{} wrt {}'.format(y,wrt))
  plt.legend(loc=0)
  plt.ylabel('sum of {}'.format(y))
  plt.xlabel(wrt)
  plt.show()

def hist(inpt,col,bins,min_lim,max_lim,merged=False):
  concat = df_concat(inpt,col,merged).iloc[:,0].compute()
  concat.plot(kind='hist',bins=bins,figsize=(15, 5),title='histogram of: {}'.format(col), facecolor='navajowhite', edgecolor='orange')
  plt.xlim((min_lim,max_lim))
  plt.show()

def valueCounts(inpt,cols,lim=None):
  charts = {}
  for i in cols:
    if lim:
      feature_count = inpt[i].value_counts(dropna=False).reset_index()[:lim].rename(columns={i: 'count', 'index': i})
    else:
      feature_count = inpt[i].value_counts(dropna=False).reset_index().rename(columns={i: 'count', 'index': i})

    chart = alt.Chart(feature_count).mark_bar().encode(
      y=alt.Y(f"{i}:N", axis=alt.Axis(title=i)),
      x=alt.X('count:Q', axis=alt.Axis(title='Count')),
      tooltip=[i, 'count'],
    ).properties(title=f"Counts of {i}", width=800)
    charts[i] = chart
  return reduce(operator.and_, list(charts.values()))

def dist_plot(inpt1,inpt2,train_col,test_col,merged=[False,False]):
  # plot dist curves for train and test data for the given column name
  concat_train = df_concat(inpt1,train_col,merged[0]).compute()
  concat_test = df_concat(inpt2,test_col,merged[1]).compute()

  fig, ax = plt.subplots(figsize=(10, 10))
  sns.distplot(concat_train.dropna(), color='green', ax=ax).set_title(train_col, fontsize=16)
  sns.distplot(concat_test.dropna(), color='purple', ax=ax).set_title(test_col, fontsize=16)
  plt.xlabel(train_col, fontsize=15)
  plt.legend(['train', 'test'])
  plt.show()

def pie(inpt,col,merged=False):
  client = Client()
  concat = client.compute(delayed(df_concat)(inpt,col,merged).iloc[:,0]).result()
  client.close()
  plt.figure(figsize=(10,10))
  labels = (np.array(concat.index))
  sizes = (np.array((concat / concat.sum())*100))
  plt.pie(sizes, labels=labels,
          autopct='%1.1f%%', startangle=200)
  plt.title("Departments distribution", fontsize=15)
  plt.show()

def correlations(df,specific_col=None):
  if specific_col:
    corrs = df.corr()[specific_col].sort_values()
    # Display correlations
    print('Most Positive Correlations:\n', corrs.tail(15))
    print('\nMost Negative Correlations:\n', corrs.head(15))
  else:
    corrs = df.corr()
    # Heatmap of correlations
    sns.heatmap(corrs, cmap = plt.cm.RdYlBu_r, vmin = -0.25, annot = True, vmax = 0.9)
    plt.title('Correlation Heatmap');
    return plt

def missmap(list_cols): # timestamp , cols , target , group_param(graph num)
  print(list_cols[0].columns[0])
  df_concat = dd.concat(list_cols,axis=1).set_index(list_cols[0].columns[0]).compute()
  print('df_concat.columns',df_concat.columns)
  # print(df_concat.iloc[[999999]])
  # Plot missing values per building/meter
  f,a=plt.subplots(1,4)
  for meter in np.arange(len(df_concat[df_concat.columns[2]].unique())):
    df = df_concat[df_concat[df_concat.columns[2]]==meter].copy().reset_index()
    print('df.columns',df.columns)
    df[df_concat.index.name] = pd.to_datetime(df[df_concat.index.name])
    # df[df_concat.index.name] = df[df_concat.index.name].astype(int)
    df[df_concat.index.name] -= df[df_concat.index.name].min()
    df[df_concat.index.name] = (df[df_concat.index.name].dt.total_seconds() / 3600).astype(int)
    # print(len(df_concat[df_concat.columns[0]].unique()))
    missmap = np.empty((len(df_concat[df_concat.columns[0]].unique()), df[df_concat.index.name].max()+1))
    missmap.fill(np.nan)
    for l in df.values:
      if l[3]!=meter:continue
      missmap[int(l[1]), int(l[0])] = 0 if l[2]==0 else 1
    a[meter].set_title(f'meter {meter:d}')
    sns.heatmap(missmap, cmap='Paired', ax=a[meter], cbar=False)
  plt.show()
  # Legend:
  # * X axis: hours elapsed since Jan 1st 2016, for each of the 4 meter types
  # * Y axis: building_id
  # * Brown: meter reading available with non-zero value
  # * Light blue: meter reading available with zero value
  # * White: missing meter reading

def cat_diff(df1,df2,cols):
  for col in cols:
    yield ('csv',pd.Series(list(set(df1[col])-set(df2[col]))).to_frame(col),'first_'+col+'_unique')
    yield ('csv',pd.Series(list(set(df2[col])-set(df1[col]))).to_frame(col),'second_'+col+'_unique')


