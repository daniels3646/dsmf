import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-b', action='store_true', help='save output df as html file')
parser.add_argument('-v', action='store', help='run a viz func')
group = parser.add_mutually_exclusive_group()
group.add_argument('-m', nargs='?', const='all', help='delimited list input', type=str)
group.add_argument('-i', nargs='?', const='all', help='delimited list input', type=str)
group.add_argument('-e', nargs='?', const='all', help='run pandas profiling', type=str)
group.add_argument('-o', nargs='?', const='all', help='output csv', type=str)
args, unknown_args = parser.parse_known_args()
