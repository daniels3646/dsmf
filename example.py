from metaflow import FlowSpec, step, batch, IncludeFile, Flow, get_metadata, metadata, Run , conda_base
import sys
import dsmf
from dsmf.util import FeatureUnionReframer as union
from dsmf.util import new_pipeline as pipe
from dsmf.util import addFeatureNames as afn
from dsmf.util import columnTransformerReframer as ct
from dsmf.util import PassthroughTransformer as passthru
from dsmf import util,viz_util,model_util
import os
import csv
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import time
from functools import partial
import sklearn.preprocessing as pre
from sklearn.impute import SimpleImputer
from sklearn.compose import make_column_transformer
from sklearn.pipeline import make_pipeline, make_union, Pipeline
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection import StratifiedKFold, KFold, RepeatedKFold, GroupKFold, GridSearchCV, train_test_split, TimeSeriesSplit
from sklearn import metrics
from sklearn import linear_model
import argparse
from sklearn.metrics import roc_auc_score
from pandas.api.types import is_numeric_dtype
import category_encoders as ce
import datetime

class dt1(model_util.Func):
  def __init__(self):
    super().__init__(lambda x: (((datetime.datetime.today() - x.purchase_date).dt.days//30) + x.month_lag).to_frame('month_diff'))

class dt2(model_util.Func):
  def __init__(self):
    super().__init__(lambda x: x.iloc[:,0].dt.month.to_frame('purchase_month'))

clean_dt = ct.retain(
  (dt1(),['purchase_date','month_lag']),
  (dt2(),['purchase_date']),
  (passthru(),['purchase_date','month_lag'])
)

class agg_cardid(model_util.Func):
  def __init__(self,key_cols,cat_cols,num_cols):
    super().__init__(lambda x: aggregate_transactions(x,key_cols,cat_cols,num_cols))

def aggregate_transactions(history,key_cols,cat_cols,num_cols):
  # history.loc[:, 'purchase_date'] = pd.DatetimeIndex(history['purchase_date']).\
                   # astype(np.int64) * 1e-9
  keys = {key: ['nunique'] for key in key_cols}
  cat = {key: ['mean'] for key in cat_cols}
  num = {key: ['sum', 'mean', 'max', 'min', 'std'] for key in num_cols}
  # dt = {
  # 'purchase_month': ['mean', 'max', 'min', 'std'],
  # 'purchase_date': [np.ptp, 'min', 'max'],
  # 'month_lag': ['mean', 'max', 'min', 'std'],
  # 'month_diff': ['mean']
  # }
  conc = keys.copy()
  conc.update(cat)
  conc.update(num)
  # conc.update(dt)

  agg_history = history.groupby(['card_id_oe']).agg(conc)
  agg_history.columns = ['_'.join(col).strip() for col in agg_history.columns.values]
  agg_history.reset_index(inplace=True)

  # df = (history.groupby('card_id_oe')
     # .size()
     # .reset_index(name='transactions_count'))

  # agg_history = pd.merge(df, agg_history, on='card_id_oe', how='left')

  return agg_history

# @conda_base(libraries={'scikit-learn':'0.22','numpy':'1.17.4','scipy':'1.1.0','argparse':'1.4.0','parsedatetime':'2.4'}, python='3.6.8')
class Elo(FlowSpec):

  def read_data(self,input_file):
    #installThings()
    # importThings()
    df = pd.read_csv(input_file)
    df['first_active_month'] = pd.to_datetime(df['first_active_month'])
    df['elapsed_time'] = (datetime.date(2018, 2, 1) - df['first_active_month'].dt.date).dt.days
    return df

  # @batch(memory=4000, cpu=2)
  @step
  def start(self):
    new_merchant_transactions = 'new_merchant_transactions.csv'
    historical_transactions = 'historical_transactions.csv'
    sub = 'sample_submission.csv'

    self.files = [(new_merchant_transactions,'new'),
        (historical_transactions,'hist')]

    self.train = self.read_data('../input/train.csv')
    self.test = self.read_data('../input/test.csv')

    self.next(self.loading, foreach='files')

  # @batch(memory=4000, cpu=2)
  @step
  def loading(self):
    file_ = '../input/'+self.input[0]
    self.file_name = self.input[1]
    self.df = util.reduce_mem_usage_sd(pd.read_csv(file_,parse_dates=['purchase_date']),verbose=True,obj_to_cat=True)
    self.df['category_2'] = self.df['category_2'].fillna(0)
    self.next(self.loading_join)

  @batch(memory=30000, cpu=2)
  @step
  def loading_join(self, inputs):

    self.trsc = {inp.file_name: inp.df \
                for inp in inputs}
    self.merge_artifacts(inputs,exclude=['df','file_name'])

# | authorized_flag      | category       |
# | card_id              | category       |
# | city_id              | int16          |
# | category_1           | category       |
# | installments         | int16          |
# | category_3           | category       |
# | merchant_category_id | int16          |
# | merchant_id          | category       |
# | month_lag            | int8           |
# | purchase_amount      | float64        |
# | purchase_date        | datetime64[ns] |
# | category_2           | float16        |
# | state_id             | int8           |
# | subsector_id         | int8           |

# | category_2           |      2.65286e+06 |  9.1125   |
# | category_3           | 178159           |  0.61197  |
# | merchant_id          | 138481           |  0.475678 |
# | subsector_id         |      0           |  0        |
# | state_id             |      0           |  0        |
# | purchase_date        |      0           |  0        |
# | purchase_amount      |      0           |  0        |
# | month_lag            |      0           |  0        |
# | merchant_category_id |      0           |  0        |
# | installments         |      0           |  0        |
# | category_1           |      0           |  0        |
# | city_id              |      0           |  0        |
# | card_id              |      0           |  0        |
# | authorized_flag      |      0           |  0        |

# authorized_flag                2
# card_id                   325540
# city_id                      308
# category_1                     2
# installments                  15
# category_3                     3
# merchant_category_id         327
# merchant_id               326311
# month_lag                     14
# purchase_amount           215014
# purchase_date           16395300
# category_2                     5
# state_id                      25
# subsector_id                  41

    self.next(self.cleaning_split)

  @batch(memory=30000, cpu=2)
  @step
  def cleaning_split(self):
    # self.cat_cols = list(self.trsc['hist'].select_dtypes(['category']).columns)
    self.cat_cols = ['card_id','category_3','merchant_id','category_2'] # category_2 should be in the num_cols
    self.num_cols = ['installments','month_lag','purchase_amount']
    self.key_cols = ['city_id','merchant_category_id','state_id','subsector_id']
    # self.num_cols = list(self.trsc['hist'].select_dtypes(['number']).columns)
    self.boolean_cols = ['category_1','authorized_flag']
    self.datetime_cols = ['purchase_date']

    self.clean_cat = pipe([
      util.change_type({'category_2':np.int8}),
      afn(SimpleImputer,args={'strategy':'constant'}),
      ct.retain(
        (ce.one_hot.OneHotEncoder(return_df=True,use_cat_names=True),['category_3']),
        (afn(pre.OrdinalEncoder,suffix='_oe'),['card_id', 'merchant_id','category_2','category_1','authorized_flag'])
      )
    ])

    self.next(self.cleaning_hist,self.cleaning_new)


  @batch(memory=30000, cpu=2)
  @step
  def cleaning_hist(self):
    self.hist_clean_dt = clean_dt.fit_transform(self.trsc['hist'][['purchase_date','month_lag']])
    self.hist_clean_cat = self.clean_cat.fit_transform(self.trsc['hist'][self.cat_cols+self.boolean_cols])
    self.hist_clean_key = passthru().fit_transform(self.trsc['hist'][self.key_cols])
    self.hist_clean_bool = passthru().fit_transform(self.trsc['hist'][self.boolean_cols])
    self.hist_clean_num = passthru().fit_transform(self.trsc['hist'][self.num_cols])
    self.next(self.cleaning_join)

  @batch(memory=30000, cpu=2)
  @step
  def cleaning_new(self):
    self.new_clean_dt = clean_dt.fit_transform(self.trsc['new'][['purchase_date','month_lag']])
    self.new_clean_cat = self.clean_cat.fit_transform(self.trsc['new'][self.cat_cols+self.boolean_cols])
    self.new_clean_key = passthru().fit_transform(self.trsc['new'][self.key_cols])
    self.new_clean_bool = passthru().fit_transform(self.trsc['new'][self.boolean_cols])
    self.new_clean_num = passthru().fit_transform(self.trsc['new'][self.num_cols])
    self.next(self.cleaning_join)

  @batch(memory=30000, cpu=2)
  @step
  def cleaning_join(self, inputs):
    self.merge_artifacts(inputs,exclude=['clean_cat','trsc'])
    self.next(self.fe_split)

  @batch(memory=30000, cpu=2)
  @step
  def fe_split(self):
    self.next(self.fe_hist,self.fe_new)

  @batch(memory=50000, cpu=2)
  @step
  def fe_hist(self):
    concat = pd.concat([self.hist_clean_cat,self.hist_clean_key,self.hist_clean_num],axis=1)
    self.hist_concat_agg = agg_cardid(self.hist_clean_key.columns,self.hist_clean_cat.columns,self.hist_clean_num.columns).fit_transform(concat)
    self.next(self.fe_join)

  @batch(memory=50000, cpu=2)
  @step
  def fe_new(self):
    concat = pd.concat([self.new_clean_cat,self.new_clean_key,self.new_clean_num],axis=1)
    self.new_concat_agg = agg_cardid(self.new_clean_key.columns,self.new_clean_cat.columns,self.new_clean_num.columns).fit_transform(concat)
    self.next(self.fe_join)

  @batch(memory=50000, cpu=2)
  @step
  def fe_join(self, inputs):
    self.merge_artifacts(inputs,exclude=['hist_clean_cat','hist_clean_num','hist_clean_key','hist_clean_bool','hist_clean_dt','new_clean_cat','new_clean_num','new_clean_key','new_clean_bool','new_clean_dt'])
    self.next(self.end)

  @step
  def end(self):
    pass

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b', action='store_true', help='save output df as html file')
  parser.add_argument('-v', action='store', help='run a viz func')
  group = parser.add_mutually_exclusive_group()
  group.add_argument('-m', nargs='?', const='all', help='delimited list input', type=str)
  group.add_argument('-i', nargs='?', const='all', help='delimited list input', type=str)
  group.add_argument('-e', nargs='?', const='all', help='run pandas profiling', type=str)
  group.add_argument('-o', nargs='?', const='all', help='output csv', type=str)

  start_time = time.time()

  args, unknown_args = parser.parse_known_args()
  if unknown_args:
    Elo()

  html = True if args.b else False
  selected = args.i or args.m or args.e or args.o or 'custom'
  t_split = selected.split(',')
  lst = [str(item) for item in t_split[2:]] if selected != 'custom' else None
  if args.i:
    viz_util.basic_info(*t_split[:2],cols=lst,html=html)
  elif args.m:
    viz_util.minimal(*t_split[:2],cols=lst)
  elif args.e:
    viz_util.extensive_info(*t_split[:2],cols=lst)
  elif args.o:
    viz_util.save_csv(*t_split[:2],cols=lst)
  elif args.v:
    viz_util.save(locals()[args.v]) # custom
  elapsed_time = time.time() - start_time
  print("Elapsed time: {}".format(util.hms_string(elapsed_time)))
